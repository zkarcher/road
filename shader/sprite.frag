precision mediump float;
precision mediump int;
uniform float colorLoops;
uniform sampler2D surfaceTexture;
uniform float txScale;
uniform vec2 txScroll;
uniform float txHardEdge;

varying vec2 vSurface_uv;
varying vec3 v_color;
//varying vec2 vScreenPos;

void main()	{
	vec2 surface = texture2D( surfaceTexture, vSurface_uv ).ra;	// 1 color, and alpha
	if( surface.y < 0.5 ) discard;
	gl_FragColor = vec4( v_color * surface.x, surface.y );
}
