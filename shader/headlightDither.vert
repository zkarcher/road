precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
attribute vec3 position;
attribute vec2 uv;
uniform float txScale;
uniform float headlightScale;
uniform float distanceY;
uniform float distanceScale;
uniform float travelOffset;
uniform vec2 steering;	// which direction the headlight is pointing
uniform float ditherTunnel;
uniform sampler2D colorTexture;

varying vec2 v_spriteUV;
varying vec2 v_headlightUV_far;
varying vec2 v_headlightUV_near;
varying vec4 v_color;
varying vec4 v_hilite;
varying vec2 v_ditherUV;

void main()	{
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
	v_spriteUV = uv;
	float useHeadlightScale = headlightScale + distanceScale * position.z;

	// Precompute gl_FragCoord.z equivalent in vertexShader, for faster colorTexture sampling
	// Vertex in clip-space:
	vec4 fake_frag_coord = (modelViewMatrix * vec4(position,1.0)); // Range:   [-w,w]^4

	// Vertex in NDC-space
	fake_frag_coord.xyz /= fake_frag_coord.w;	// Rescale: [-1,1]^3
	fake_frag_coord.w    = 1.0 / fake_frag_coord.w;	// Invert W

	vec2 colorUV = vec2( travelOffset, 1.0 - fake_frag_coord.z );
	v_color = texture2D( colorTexture, colorUV );
	v_hilite = vec4( 1.0, 1.0, 1.0, 1.0 );

	v_headlightUV_far = (position.xy - vec2(0.0,position.z*distanceY)) * useHeadlightScale + vec2(0.5,0.48) + steering*position.z;
	v_headlightUV_near = (position.xy - vec2(0.0,position.z*distanceY)) * useHeadlightScale * 0.85 + vec2(0.5,0.48) + steering*position.z*0.85;

	v_ditherUV = vec2( position.xy * txScale ) * (1.0 + position.z*ditherTunnel);
}
