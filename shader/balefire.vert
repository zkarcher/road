precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
attribute vec3 position;
attribute vec2 uv;

uniform float txScale;
uniform vec2 txScroll;
uniform float ripplePhase;
uniform float rippleFreq;
uniform float rippleStrength;

varying vec2 v_uv;
varying vec2 v_ditherUV;
varying float v_rippleTerm;
varying float v_rippleStrength;
//"varying vec2 vScreenPos;

void main()	{
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
	v_uv = uv;
	v_ditherUV = position.xy * txScale + txScroll;
	v_rippleTerm = uv.y * rippleFreq + ripplePhase;
	v_rippleStrength = uv.y * rippleStrength;
	//vScreenPos = gl_Position.xy;
}
