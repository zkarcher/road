precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;
uniform sampler2D colorTexture;
uniform float travelOffset;
attribute vec3 position;
attribute vec2 uv;

varying vec2 vSurface_uv;
varying vec3 v_color;
//varying vec2 vScreenPos;

void main()	{
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );
	vSurface_uv = uv;
	//vScreenPos = gl_Position.xy;

	// Precompute gl_FragCoord.z equivalent in vertexShader, for faster colorTexture sampling
	// Vertex in clip-space:
	vec4 fake_frag_coord = (modelViewMatrix * vec4(position,1.0));	// Range:   [-w,w]^4

	// Vertex in NDC-space
	fake_frag_coord.xyz /= fake_frag_coord.w; // Rescale: [-1,1]^3
	fake_frag_coord.w    = 1.0 / fake_frag_coord.w; // Invert W

	// Vertex in window-space
	fake_frag_coord.xyz *= vec3 (0.5) + vec3 (0.5);	// Rescale: [0,1]^3
	//fake_frag_coord.xy  *= window;	// Scale and Bias for Viewport

	//vec2 colorUV = vec2( travelOffset, 1.0-fake_frag_coord.z );	//<-- BROKEN, all sprites black
	vec2 colorUV = vec2( travelOffset, fake_frag_coord.z );
	v_color = texture2D( colorTexture, colorUV ).rgb;
}
