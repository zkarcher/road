precision mediump float;
precision mediump int;
uniform float colorLoops;
uniform sampler2D colorTexture;
uniform float travelOffset;
uniform sampler2D surfaceTexture;

varying vec2 vSurface_uv;

void main()	{
	vec2 surface = texture2D( surfaceTexture, vSurface_uv ).ra;	// 1 color, and alpha
	vec2 colorUV = vec2( travelOffset + surface.x * colorLoops, 1.0 );
	gl_FragColor = vec4( texture2D( colorTexture, colorUV ).rgb, surface.y );
}
