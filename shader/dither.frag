precision mediump float;
precision mediump int;
uniform float overlap;
uniform sampler2D ditherTexture;
uniform sampler2D colorTexture;
uniform float octaveAlpha0;
uniform float octaveAlpha1;
uniform bool isRoad;
uniform sampler2D surfaceTexture;	// for: surface brightness Y

varying vec2 vSurface_uv;	// for: surface brightness Y

//varying float vPerspective;	// fake perspective distortion curve, z^3 or whatever
varying float vPerspectiveMoving;

varying vec2 vOctave0_uv;
varying vec2 vOctave1_uv;

void main()	{
	float dither = texture2D( ditherTexture, vOctave0_uv ).r * octaveAlpha0;
	dither += texture2D( ditherTexture, vOctave1_uv ).r * octaveAlpha1;

	float gr8 = vPerspectiveMoving + dither * overlap;

	if (isRoad == true) {	// "surface brightness Y"
		float surface = texture2D( surfaceTexture, vSurface_uv ).r;
		gl_FragColor = texture2D( colorTexture, vec2( gr8, surface - fract(gr8)*0.25 ) );

	} else {	// "distance Y"
		gl_FragColor = texture2D( colorTexture, vec2( gr8, 1.0 - gl_FragCoord.z ) );
	}
}
