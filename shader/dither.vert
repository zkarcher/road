precision mediump float;
precision mediump int;
uniform mat4 modelViewMatrix;
uniform mat4 projectionMatrix;

attribute vec3 position;
attribute vec2 uv;	// for: surface brightness Y

//"uniform vec2 octaveCenter;
uniform float colorLoops;
uniform vec3 octaveScroll;
uniform float octaveSize0;
uniform float octaveSize1;
uniform float txScale;
uniform float skewMult;	// -1=skew, 1=normal rotate
uniform vec2 farCrunch;
uniform float depthCurve;
uniform float travelOffset;

varying float vPerspectiveMoving;
varying vec2 vOctave0_uv;
varying vec2 vOctave1_uv;
varying vec2 vSurface_uv;	// for: surface brightness Y



void main()	{
	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );

	float perspective = clamp( -gl_Position.z, 0.0, 1.0 );
	perspective = pow( perspective, depthCurve );	// weight nearer/further
	vPerspectiveMoving = (travelOffset + perspective) * colorLoops;

	float rollSin = sin( octaveScroll.z );
	float rollCos = cos( octaveScroll.z );

	// gl_Position.z: Near==-1, far==0
	vec2 crunch = vec2(1.0,1.0) + farCrunch * (1.0-perspective);	// whoa weird near distortion

	float octaveScale0 = txScale * octaveSize0;
	vOctave0_uv = position.xy * octaveScale0;
	vOctave0_uv = vec2( vOctave0_uv.x * rollCos - vOctave0_uv.y * rollSin * skewMult, vOctave0_uv.x * rollSin + vOctave0_uv.y * rollCos );
	vOctave0_uv *= crunch;
	vOctave0_uv += octaveScroll.xy;

	float octaveScale1 = txScale * octaveSize1;
	vOctave1_uv = position.xy * octaveScale1;
	vOctave1_uv = vec2( vOctave1_uv.x * rollCos - vOctave1_uv.y * rollSin * skewMult, vOctave1_uv.x * rollSin + vOctave1_uv.y * rollCos );
	vOctave1_uv *= crunch;
	vOctave1_uv += octaveScroll.xy;
	vSurface_uv = uv;	// for: surface brightness Y
}
