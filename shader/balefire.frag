precision mediump float;
precision mediump int;
uniform float colorLoops;
uniform sampler2D colorTexture;
uniform float travelOffset;
uniform sampler2D fireTexture;
uniform sampler2D ditherTexture;
uniform float txHardEdge;

varying vec2 v_uv;
varying vec2 v_ditherUV;
varying float v_rippleTerm;
varying float v_rippleStrength;

void main()	{
	float ripple = v_rippleStrength * sin( v_rippleTerm );
	vec2 uvRipple = vec2( v_uv.x + ripple, v_uv.y );
	float fireValue = texture2D( fireTexture, uvRipple ).r;
	//float ditherValue = 1.0 - texture2D( ditherTexture, gl_FragCoord.xy * txScale + txScroll ).r;
	float ditherValue = 1.0 - texture2D( ditherTexture, v_ditherUV ).r;
	if( fireValue < ditherValue ) discard;

	float ditherAlpha = clamp( (fireValue-ditherValue)*txHardEdge, 0.0, 1.0 );
	gl_FragColor = vec4( texture2D( colorTexture, vec2( travelOffset+(fireValue*colorLoops), gl_FragCoord.z ) ).rgb, ditherAlpha );
}
