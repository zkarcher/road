precision mediump float;
precision mediump int;
uniform sampler2D spriteTexture;
uniform sampler2D headlightTexture;
uniform sampler2D ditherTexture;
//"uniform sampler2D colorTexture;

varying vec2 v_spriteUV;
varying vec2 v_headlightUV_far;
varying vec2 v_headlightUV_near;
varying vec4 v_color;
varying vec4 v_hilite;
varying vec2 v_ditherUV;

void main()	{

	vec2 sprite = vec2( texture2D( spriteTexture, v_spriteUV ).ra );
	if( sprite.y < 0.05 ) discard;

	float dither = texture2D( ditherTexture, v_ditherUV ).r;

	//" vec4 color = texture2D( colorTexture, v_colorUV );

	// Fake bump mapping, sorta. Texture .r lerps _far and _near versions
	// of v_headlightUV, appears as if the light is projected on a surface
	// with some depth & relief.
	vec4 headlight = texture2D( headlightTexture, mix( v_headlightUV_far, v_headlightUV_near, sprite.x ) );

	// Backlight: Weight darker so it doesn't interfere with headlight as much.
	float backlight = (1.0-sprite.x);
	backlight *= backlight;

	// Plasticine 2-color glow region
	//" gl_FragColor = vec4( (v_color.rgb * (headlight.r*2.0)) + (v_hilite.rgb * clamp( headlight.r*2.0-1.0, 0.0, 1.0 ) ), sprite.y );
	gl_FragColor = vec4( dither * ((v_hilite.rgb * headlight.r * 4.0) + (v_color.rgb * backlight)), sprite.y );

	/*
	 if( headlight.r < 0.5 ) {
	  gl_FragColor = ( v_color * (headlight.r*2.0) ) * sprite.zzzy;
	 } else {
	  gl_FragColor = ( v_color + v_hilite * (headlight.r*2.0-1.0) ) * sprite.zzzy;
	 }
	*/
	//" gl_FragColor = vec4( sprite.xxxy );",	// z-fighting test
}
