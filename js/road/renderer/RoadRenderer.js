function RoadRenderer(road, settings) {
	var self = this;

	var gl_scene, camera, renderer, stats;
	var _isReady = false;

	var groundObj, groundGeom, groundPositions, groundIndexes, groundShader;
	var roadObj, roadGeom, roadPositions, roadIndexes, roadUVs, roadShader;
	var roadMat;

 	var moonShader = null, moon = null;
	self.getMoon = function() {
		return moon;
	}

	var spriteObj, spriteGeom, spritePositions, spriteIndexes, spriteUVs;
	var spriteTx, spriteShader;

	var fireObj, fireGeom, firePositions, fireIndexes, fireUVs, fireTx;
	var fireShader;

	var headlightObj, headlightGeom, headlightPositions, headlightIndexes, headlightUVs;
	var headlightShader;

	var bg_U = 0, bg_V = 0;
	var lastPanY = 0;

	// Optimization: cache ideal flat Z distances for each row
	var flatZ_ar = new Float32Array(MAX_RENDER_HEIGHT);
	//var diffZ_ar = new Float32Array(MAX_RENDER_HEIGHT);

	// As the road curves & hills, octave TX scrolls & rolls (.z is roll)
	var groundOctaveScrollAdder = new THREE.Vector3( 0, 0, 0 );
	var groundTravelOffset = 0;
	var roadOctaveScrollAdder = new THREE.Vector3( 0, 0, 0 );
	var roadTravelOffset = 0;

	// 0..1, octave textures grow as player moves forward. Wraps around.
	var groundOctave = 0;
	var roadOctave = 0;

	var roadProgress=0.0, txPhase=0.0;

	self.isReady = function(){
		return _isReady;
	}

	self.getCamera = function(){ return camera; }

	self.setSize = function(w, h) {
		renderer.setSize(w, h);
	}

	self.onWindowResize = function(){
		if( groundShader ) groundShader.onWindowResize();
		if( roadShader ) roadShader.onWindowResize();
	}

	self.init3D = function() {
		var view = road.getView();
		gl_scene = new THREE.Scene();
		camera = new THREE.OrthographicCamera();
		//gl_scene.add( camera );

		renderer = new THREE.WebGLRenderer( {
			canvas:document.getElementById("threeJS_canvas"),
			antialias:true,
			//alpha:true
		} );
		renderer.setPixelRatio(window.devicePixelRatio ? window.devicePixelRatio : 1);

		document.body.appendChild( renderer.domElement );

		stats = new Stats();
		stats.domElement.style.position = 'absolute';
		stats.domElement.style.top = '0px';
		document.body.appendChild( stats.domElement );

		var promises = [];

		groundShader = new DitherShader( "G:", "ground", settings.getProps() );

		promises.push( groundShader.init().then(
			function() {
				groundPositions = new Float32Array(MAX_RENDER_HEIGHT * 4 * 3 );	// 4 points per quad, XYZ
				groundIndexes = new Uint16Array(MAX_RENDER_HEIGHT * 2 * 3 );	// 2 tris per quad, 3 points per tri
				//groundUVs = new Float32Array( RENDER_HEIGHT * 4 * 2 );	// 4 points per quad, UV
				for( var i=0; i<MAX_RENDER_HEIGHT; i++ ) {
					//
					//  0---2
					//  | / |
					//  1---3
					//
					var atPosition = i * 4;
					var idx = i * 6;
					groundIndexes[idx  ] = atPosition;
					groundIndexes[idx+1] = atPosition+1;
					groundIndexes[idx+2] = atPosition+2;
					groundIndexes[idx+3] = atPosition+1;
					groundIndexes[idx+4] = atPosition+2;
					groundIndexes[idx+5] = atPosition+3;

					var pos = i * 12;
					groundPositions[pos  ] = groundPositions[pos+3] = -MAX_RENDER_WIDTH/2;	// X: left
					groundPositions[pos+6] = groundPositions[pos+9] = MAX_RENDER_WIDTH/2;	// X: right
					groundPositions[pos+1] = groundPositions[pos+7] = -(MAX_RENDER_HEIGHT/2) + i + 1;	// Y: top
					groundPositions[pos+4] = groundPositions[pos+10] = -(MAX_RENDER_HEIGHT/2) + i;	// Y: bottom
					groundPositions[pos+2] = groundPositions[pos+5] = groundPositions[pos+8] = groundPositions[pos+11] = 0;	// Z
				}

				groundGeom = new THREE.BufferGeometry();

				groundGeom.addAttribute( 'position', new THREE.BufferAttribute( groundPositions, 3 ).setDynamic(true) );

				//groundGeom.addAttribute( 'index', new THREE.BufferAttribute( groundIndexes, 1 ) );
				groundGeom.setIndex(new THREE.BufferAttribute( groundIndexes, 1 ));

				groundGeom.addGroup({ start:0, count:0, materialIndex:0 });

				groundObj = new THREE.Mesh( groundGeom, groundShader.material );
				gl_scene.add( groundObj );
				groundObj.renderOrder = 3;
			}
		));

		//
		//  PAVEMENT (ROAD)
		//
		roadShader = new DitherShader( "R:", "road", settings.getProps() );
		promises.push( roadShader.init().then(
			function() {
				roadPositions = new Float32Array(MAX_RENDER_HEIGHT * 4 * 3 * MAX_ROAD_SPLITS);	// 4 points per quad, XYZ
				roadIndexes = new Uint16Array(MAX_RENDER_HEIGHT * 2 * 3 * MAX_ROAD_SPLITS);	// 2 tris per quad, 3 points per tri
				roadUVs = new Float32Array(MAX_RENDER_HEIGHT * 4 * 2 * MAX_ROAD_SPLITS);	// 4 points per quad, UV

				for( var i = 0; i < MAX_RENDER_HEIGHT * MAX_ROAD_SPLITS; i++ ) {
					//
					//  0---2
					//  | / |
					//  1---3
					//
					var atPosition = i * 4;
					var idx = i * 6;
					roadIndexes[idx  ] = atPosition;
					roadIndexes[idx+1] = atPosition+1;
					roadIndexes[idx+2] = atPosition+2;
					roadIndexes[idx+3] = atPosition+1;
					roadIndexes[idx+4] = atPosition+2;
					roadIndexes[idx+5] = atPosition+3;
				}

				roadGeom = new THREE.BufferGeometry();

				roadGeom.addAttribute( 'position', new THREE.BufferAttribute( roadPositions, 3 ).setDynamic(true) );

				//roadGeom.addAttribute( 'index', new THREE.BufferAttribute( roadIndexes, 1 ) );
				roadGeom.setIndex(new THREE.BufferAttribute( roadIndexes, 1 ));

				// Sadly, I can't name 'uv' to 'surfaceUV', doesn't get passed through to shader :(
				roadGeom.addAttribute( 'uv', new THREE.BufferAttribute( roadUVs, 2 ).setDynamic(true) );
				roadGeom.addGroup({ start:0, count:0, materialIndex:0 });

				roadObj = new THREE.Mesh( roadGeom, roadShader.material );
				gl_scene.add( roadObj );
				roadObj.renderOrder = 2;
			}
		));

		// Sprite sheet generated at: http://www.leshylabs.com/apps/sstool/
		var spritePromises = [
			shaderUtil_loadTexture('p/sprites/spritesheet.png'),
			shaderUtil_loadTexture('p/shader/' + settings.get('H:'+HEADLIGHT_TEXTURE)),
			shaderUtil_loadTextFile('p/sprites/sprites.txt'),
		];

		promises.push( Promise.all(spritePromises).then(
			function(ar) {
				spriteTx = ar[0];
				headlightTx = ar[1];
				spriteData = ar[2];

				spriteTx.minFilter = spriteTx.magFilter = THREE.NearestFilter;
				var tx_w = spriteTx.image.width;
				var tx_h = spriteTx.image.height;

				var lines = spriteData.split( /[\n\r]+/ );

				for( var li=0; li<lines.length; li++ ) {
					var tokens = lines[li].split( ',' );
					if( tokens.length < 5 ) continue;	// sanity check

					var obj = {
						x:parseInt(tokens[1]), y:parseInt(tokens[2]),
						w:parseInt(tokens[3]), h:parseInt(tokens[4])
					};
					obj['uvs'] = new Float32Array([
						obj.x / tx_w,         1.0 - (obj.y / tx_h),
						(obj.x+obj.w) / tx_w, 1.0 - (obj.y / tx_h),
						obj.x / tx_w,         1.0 - ((obj.y+obj.h) / tx_h),
						(obj.x+obj.w) / tx_w, 1.0 - ((obj.y+obj.h) / tx_h)
					]);
					spriteAtlas[ tokens[0] ] = obj;
				}

				spritePositions = new Float32Array( MAX_SPRITES * 4 * 3 );
				spriteIndexes = new Uint16Array( MAX_SPRITES * 4 * 3 );
				spriteUVs = new Float32Array( MAX_SPRITES * 4 * 2 );

				for( var i=0; i<MAX_SPRITES; i++ ) {
					//
					//  0---2
					//  | / |
					//  1---3
					//
					var atPosition = i * 4;
					var idx = i * 6;

					spriteIndexes[idx  ] = atPosition;
					spriteIndexes[idx+1] = atPosition+1;
					spriteIndexes[idx+2] = atPosition+2;
					spriteIndexes[idx+3] = atPosition+1;
					spriteIndexes[idx+4] = atPosition+2;
					spriteIndexes[idx+5] = atPosition+3;
				}

				spriteGeom = new THREE.BufferGeometry();

				spriteGeom.addAttribute( 'position', new THREE.BufferAttribute( spritePositions, 3 ).setDynamic(true) );

				//spriteGeom.addAttribute( 'index', new THREE.BufferAttribute( spriteIndexes, 1 ) );
				spriteGeom.setIndex(new THREE.BufferAttribute( spriteIndexes, 1 ));

				spriteGeom.addAttribute( 'uv', new THREE.BufferAttribute( spriteUVs, 2 ).setDynamic(true) );

				// custom attribute: fireUV must be passed to RawShaderMaterial on creation. :P
				//                   See BalefireShader.js for example.
				//spriteGeom.addAttribute( 'fireUV', new THREE.BufferAttribute( spriteFireUVs, 2 ) );
				spriteGeom.addGroup({ start:0, count:0, materialIndex:0 });

				//var fireTx = THREE.ImageUtils.loadTexture( 'p/sprites/balefire.png' );
				spriteShader = new SpriteShader( spriteGeom, spriteTx, 'S:', settings.getProps() );
				spriteShader.init().then(
					function() {
						spriteObj = new THREE.Mesh( spriteGeom, spriteShader.material );
						spriteObj.renderOrder = 4;
						gl_scene.add( spriteObj );
					}
				)

				// Make Headlights
				headlightPositions = new Float32Array( MAX_SPRITES * 4 * 3 );
				headlightIndexes = new Uint16Array( MAX_SPRITES * 4 * 3 );
				headlightUVs = new Float32Array( MAX_SPRITES * 4 * 2 );

				for( var i=0; i<MAX_SPRITES; i++ ) {
					var atPosition = i * 4;
					var idx = i * 6;

					headlightIndexes[idx  ] = atPosition;
					headlightIndexes[idx+1] = atPosition+1;
					headlightIndexes[idx+2] = atPosition+2;
					headlightIndexes[idx+3] = atPosition+1;
					headlightIndexes[idx+4] = atPosition+2;
					headlightIndexes[idx+5] = atPosition+3;
				}

				headlightGeom = new THREE.BufferGeometry();

				headlightGeom.addAttribute( 'position', new THREE.BufferAttribute( headlightPositions, 3 ).setDynamic(true) );

				//headlightGeom.addAttribute( 'index', new THREE.BufferAttribute( headlightIndexes, 1 ) );
				headlightGeom.setIndex(new THREE.BufferAttribute( headlightIndexes, 1 ));

				headlightGeom.addAttribute( 'uv', new THREE.BufferAttribute( headlightUVs, 2 ).setDynamic(true) );

				headlightGeom.addGroup({ start:0, count:0, materialIndex:0 });

				headlightShader = new HeadlightDitherShader( headlightGeom, spriteTx, headlightTx, 'H:', settings.getProps() );
				headlightShader.init().then(
					function() {
						headlightObj = new THREE.Mesh( headlightGeom, headlightShader.material );
						headlightObj.renderOrder = 6;
						gl_scene.add( headlightObj );
					}
				)
			}
		));

		promises.push( shaderUtil_loadTexture('p/sprites/balefire.png').then(
			function(tx) {
				fireTx = tx;

				firePositions = new Float32Array( MAX_SPRITES * 4 * 3 );
				fireIndexes = new Uint16Array( MAX_SPRITES * 4 * 3 );
				fireUVs = new Float32Array( MAX_SPRITES * 4 * 2 );

				for( var i=0; i<MAX_SPRITES; i++ ) {
					var atPosition = i * 4;
					var idx = i * 6;

					fireIndexes[idx  ] = atPosition;
					fireIndexes[idx+1] = atPosition+1;
					fireIndexes[idx+2] = atPosition+2;
					fireIndexes[idx+3] = atPosition+1;
					fireIndexes[idx+4] = atPosition+2;
					fireIndexes[idx+5] = atPosition+3;

					var uvIdx = i * 8;
					fireUVs[uvIdx  ] = 0;
					fireUVs[uvIdx+1] = 1;
					fireUVs[uvIdx+2] = 1;
					fireUVs[uvIdx+3] = 1;
					fireUVs[uvIdx+4] = 0;
					fireUVs[uvIdx+5] = 0;
					fireUVs[uvIdx+6] = 1;
					fireUVs[uvIdx+7] = 0;
				}

				fireGeom = new THREE.BufferGeometry();

				fireGeom.addAttribute( 'position', new THREE.BufferAttribute( firePositions, 3 ).setDynamic(true) );

				//fireGeom.addAttribute( 'index', new THREE.BufferAttribute( fireIndexes, 1 ) );
				fireGeom.setIndex(new THREE.BufferAttribute( fireIndexes, 1 ));

				fireGeom.addAttribute( 'uv', new THREE.BufferAttribute( fireUVs, 2 ).setDynamic(true) );

				// custom attribute: fireUV must be passed to RawShaderMaterial on creation. :P
				//                   See BalefireShader.js for example.
				//spriteGeom.addAttribute( 'fireUV', new THREE.BufferAttribute( spriteFireUVs, 2 ) );
				fireGeom.addGroup({ start:0, count:0, materialIndex:0 });

				fireShader = new BalefireShader( fireGeom, fireTx, 'F:', settings.getProps() );
				fireShader.init().then(
					function() {
						fireObj = new THREE.Mesh( fireGeom, fireShader.material );
						fireObj.renderOrder = 5;
						gl_scene.add( fireObj );
					}
				);
			}
		));

		//road.setCurveChange();

		promises.push( shaderUtil_loadTexture('p/bg/bg_pixel_mountains.png').then(
			function (bgTexture) {
				bgTexture.wrapS = THREE.RepeatWrapping;
				bgTexture.wrapT = THREE.ClampToEdgeWrapping;
				bgTexture.magFilter = THREE.NearestFilter;	// when z is near, texture is blown up
				bgTexture.minFilter = THREE.NearestFilter;	// when z is far, texture is shrunk down
				var bgMat = new THREE.MeshBasicMaterial( {color:0xffffff, map:bgTexture, transparent:true, depthTest:true, depthWrite:false} );
				var bgGeom  = new THREE.PlaneGeometry( MAX_RENDER_WIDTH, MAX_RENDER_HEIGHT );
				bg = new THREE.Mesh( bgGeom, bgMat );
				bg.position.set( 0, 0, -settings.get( HORIZON_DISTANCE ) );
				gl_scene.add( bg );

				bg.renderOrder = 1;
			}
		));

		promises.push( shaderUtil_loadTexture('p/bg/moon.png').then(
			function (moonTx) {
				moonShader = new ColorCycleShader( moonTx, "M:", settings.getProps() );
				moonShader.init().then(
					function() {
						moon = new BackgroundSprite( gl_scene, moonTx, 512, 512, {shaderMaterial:moonShader.material} );
						moon.loc.z = -settings.get( HORIZON_DISTANCE );
					}
				)
			}
		));

		return Promise.all(promises).then(
			function() {
				_isReady = true;
			}
		);
	}

	function getShaderByPrefix( prefix ) {
		var shaders = {
			'G:': groundShader,
			'R:': roadShader,
			'M:': moonShader,
			'S:': spriteShader,
			'F:': fireShader,
			'H:': headlightShader,
		};
		return shaders[prefix];
	}

	self.updateDitherTexture = function( prefix ){
		var shader = getShaderByPrefix( prefix );
		if( shader ) shader.updateDitherTexture();
	}

	self.updateHeadlightTexture = function( prefix ) {
		var headlightFile = settings.get('H:'+HEADLIGHT_TEXTURE);
		shaderUtil_loadTexture('p/shader/' + headlightFile).then(
			function(headlightTx) {
				var shader = getShaderByPrefix( prefix );
				if( shader ) shader.updateHeadlightTexture( headlightTx );
			}
		);
	}

	self.updatePalette = function( prefix, doReroll ){
		var shader = getShaderByPrefix( prefix );
		if( shader ) shader.updatePalette( doReroll );
	}

	self.perFrame = function(timeMult, course, roadDelta, drive_xOffset) {
		var render_width = Math.min(MAX_RENDER_WIDTH, window.innerWidth);
		var render_height = Math.min(MAX_RENDER_HEIGHT, window.innerHeight);

		var isKeyboardDriving = road.getIsKeyboardDriving();
		var drive_speed = road.getDriveSpeed();
		var keyDirX = road.getKeyDirX();
		var keyDirY = road.getKeyDirY();

		var is1990s = (settings.get(DRAW_PRECISION) === PRECISION_1990S);
		var is1980s = (settings.get(DRAW_PRECISION) === PRECISION_1980S);

		var _bgScrollX = settings.get( BG_SCROLL_X );
		var _bgScrollY = settings.get( BG_SCROLL_Y );

		var _roadWidth = settings.get( ROAD_WIDTH );
		var _txScaleY = Math.max( 0.01, settings.get( TX_SCALE_Y ) );	// avoid divide-by-zero
		var _dxReducesZ_rads = settings.get( DX_REDUCES_Z ) * Math.PI;
		//var _dx2TxAngle_rads = settings.get( DX_2_TX_ANGLE ) * -Math.PI;
		//var _invCos2Width = settings.get( INV_COS_2_WIDTH );
		var _pitchSkew = settings.get( PITCH_SKEW );
		var _turnSteep = settings.get( TURN_STEEP );
		var _hillSteep = settings.get( HILL_STEEP );
		var _rollSteep = settings.get( ROLL_STEEP );
		var _horizonDistance = settings.get( HORIZON_DISTANCE );
		var _scaleCourse = settings.get( SCALE_COURSE );
		var _conveyorBelt = settings.get( CONVEYOR_BELT );

		var _vanishingPointMult = render_height / 2;

		for( var i=render_height-1; i>=0; i-- ) {
			// http://www.extentofthejam.com/pseudo/
			//   Z = Y_world / (Y_screen - (height_screen / 2))

			// This produces negative numbers. More negative == further away:
			flatZ_ar[i] = (render_height/2) / (-i - ((render_height/2)/_horizonDistance));
		}

		// Road rolls along
		roadProgress += roadDelta;
		txPhase += roadDelta * _conveyorBelt;
		txPhase = wrap( txPhase, 1.0 / _txScaleY );

		course.perFrame(timeMult, roadProgress, roadDelta);

		// Update RoadView: Some sprites are visible, some move beyond our view
		var view = road.getView();
		view.perFrame(timeMult, course, roadProgress);

		var sprites = view.sprites;
		var bgSprites = view.bgSprites;

		// Reusable points used for drawing road quads
		var centerPt = new THREE.Vector2();	// per row
		var widthVector = new THREE.Vector2();	// per row

		var leftPt = new THREE.Vector2();	// each split quad in rows
		var rightPt = new THREE.Vector2();	// each split quad in rows
		var lastPts_ar = [
			[new THREE.Vector2(),new THREE.Vector2()],
			[new THREE.Vector2(),new THREE.Vector2()]
		];

		// Road texture
		var roadLeftUV = new THREE.Vector2();
		var roadRightUV = new THREE.Vector2();
		var roadLastUVs_ar = [
			[new THREE.Vector2(0.0,txPhase),new THREE.Vector2(0.5,txPhase)],
			[new THREE.Vector2(0.5,txPhase),new THREE.Vector2(1.0,txPhase)]
		];

		var viewportCenter = new THREE.Vector2( render_width/2, render_height/2 );

		// Shaders
		// Scale: weight smaller, will sample at larger sizes
		var _groundTxScale = Math.pow( 1.0 - settings.get( 'G:'+TX_SCALE ), 4.0 ) * 0.25;
		var _roadTxScale = Math.pow( 1.0 - settings.get( 'R:'+TX_SCALE ), 4.0 ) * 0.25;
		var _fireTxScale = Math.pow( 1.0 - settings.get( 'F:'+TX_SCALE ), 4.0 ) * 0.25;
		var _headlightTxScale = Math.pow( 1.0 - settings.get( 'H:'+TX_SCALE ), 4.0 ) * 0.25;
		var _headlightScale = Math.pow( 1.0 - settings.get( 'H:'+HEADLIGHT_SCALE ), 4.0 ) * 0.25;

		var _groundStrafeAmt = settings.get( 'G:'+STRAFE_AMT );
		var _roadStrafeAmt = settings.get( 'R:'+STRAFE_AMT );

		var xOffset = drive_xOffset;
		var _dx = settings.get( DX );
		var _y=0, _dy=0;
		var first_dx = 0.0;

		var first_dy = settings.get( DY );
		var flatZ0 = flatZ_ar[render_height-1];
		var flatZ1 = flatZ_ar[render_height-2];
		var rp0 = (roadProgress - flatZ0) * _scaleCourse;
		var rp1 = (roadProgress - flatZ1) * _scaleCourse;

		var getStuff0 = course.get_dx_y_roll_and_splits( rp0 );
		var getStuff1 = course.get_dx_y_roll_and_splits( rp1 );
		first_dx = getStuff0.x;
		const CAMERA_PAN_Y_AMT = 0.6;
		first_dy += (getStuff0.y - getStuff1.y) * (CAMERA_PAN_Y_AMT / (flatZ0-flatZ1)) * (_hillSteep/DEFAULT_HILL_STEEP);

		var first_roll = settings.get( FIRST_ROLL );
		first_roll -= getStuff0.z * _rollSteep;	// FIXME: uncomment this
		var _roll = 0;	// changes each plane

		var subPixelY = 0.5;	// vanishing points near the row: Limit dz, and accumulate the actual subpixel changes in y
		var lastCurveY;
		var _z=0, lastZ=0;
		var lastCurveIdx = 0;

		var roadPositionAr = [];	// sorted from nearest to furthest

		var lastDrawnY_L=-9999, lastDrawnY_R=-9999;	// draw hills behind hills
		var row = render_height;
		var groundQuadsDrawn=0, roadQuadsDrawn=0;
		var isFirstRow = true;

		// Keyboard driving
		var offRoadAmt = 0.0;
		if( isKeyboardDriving ) {
			var normXOffset = drive_xOffset * (-1.0/MAX_DRIVE_STRAFE);
			const OFF_ROAD_AFTER_RATIO = 0.5;
			if( Math.abs(normXOffset) > OFF_ROAD_AFTER_RATIO ) {
				offRoadAmt = Math.max( 0.0, Math.abs(normXOffset) - OFF_ROAD_AFTER_RATIO ) / (1.0-OFF_ROAD_AFTER_RATIO);	// 1..0..0..1

				function easeOutQuad(v){ return 1.0-(1.0-v)*(1.0-v); }
				var offRoadAmt_strong = easeOutQuad( offRoadAmt );
				var offRoadAmt_weak = offRoadAmt * offRoadAmt;	// ease in quad

				// Speed drags down
				const MAX_OFF_ROAD_SPEED = MAX_DRIVE_SPEED / 4;
				if( drive_speed > MAX_OFF_ROAD_SPEED ) {
					drive_speed = lerp( drive_speed, MAX_OFF_ROAD_SPEED, offRoadAmt_strong );
				}

				const OFF_ROAD_PAN_CAMERA_AMT = 0.08;
				_dx += offRoadAmt_weak * OFF_ROAD_PAN_CAMERA_AMT * ((drive_xOffset > 1) ? 1 : -1)

				// jitter the camera
				var bumpy = offRoadAmt_strong * 5.0;

				//_dx += bumpy * randBi(0.00001) * drive_speed;	// ugh carsickness
				first_dy += bumpy * randBi(0.000025) * drive_speed;
			}
		}

		var firstRollAngle = (first_roll) * Math.PI*0.25;
		var firstRoll_sin = Math.sin( firstRollAngle );
		var firstRoll_cos = Math.cos( firstRollAngle );

		// Draw lots of planes, from the bottom of the screen up
		for( var drawAttempts=0; drawAttempts<(render_height*2); drawAttempts++ ) {
			var oldRow = row;
			row--;	// from RENDER_HEIGHT-1, decreasing to 0
			//if( row < 0 ) break;
			if( groundQuadsDrawn >= render_height ) break;

			var roadPosition = new RoadPosition();
			roadPositionAr.push( roadPosition );

			var vanishingPoint;	// Vector2
			var shrinkRatioY;	// Used to move towards vanishing point. 1.001=shrink next road segment a little, 2.0=shrink lots
			var _dz;

			// Precompute variables for roll
			var rollAngle = (first_roll + _roll) * Math.PI*0.25;	// 1.0 == 45 degrees
			var roll_sin = Math.sin( rollAngle );	// zero roll == 0.0
			var roll_cos = Math.cos( rollAngle );	// zero roll == 1.0
			var roll_cos_inv = 1.0 / roll_cos;
			var roll_tan = Math.tan( rollAngle );

			if( isFirstRow ) {
				_z = lastZ = flatZ_ar[row];
				vanishingPoint = viewportCenter;
				shrinkRatioY = 1.0;
				//_dz = 0;	// <-- fixes dx overshoot problem (angle overshoots around curves)... looks kinda boring, to be honest :P
				_dz = _z * -0.3;	// tiny bit of overshoot

			} else {
				// Based on the dy (road angle), how high is the vanishing point?
				// tan( Math.atan2() ) is unnecessary, since we're not storing angles as rotations (radians/degrees).
				// We're storing angles as dy.
				vanishingPoint = new THREE.Vector2(
					_dx * _vanishingPointMult,
					-(_dy + first_dy) * _vanishingPointMult
				);	// (this is centered around 0,0)

				// First roll changes the vanishing point. It rotates.
				rotateVec2( vanishingPoint, firstRollAngle * -0.25 * _pitchSkew );

				// Offset vanishingPoint so it's centered around the center of the viewport.
				vanishingPoint.add( viewportCenter );

				// Road shrinks towards the vanishing point at a linear rate (producing a straight-looking road).
				if( vanishingPoint.y < row ) {	// vanishing point is ABOVE this row
					shrinkRatioY = 1.0 + (1.0 / (row - vanishingPoint.y) );	// 1.001=shrink next road segment a little, 2.0=shrink lots

				} else {	// vanishing point is BELOW this row
					// The road is moving down behind previously drawn rows, and we can't draw it.
					shrinkRatioY = 1.0 + (1.0 / (vanishingPoint.y - row) );
					row += 2;	// we are computing values for the row _below_ the previous one
				}

				// When row meets the vanishing point: Well, this could be bad... The road could decide to extend out very far: (1.0 / 0.000001)
				// Not sure how to mitigate this when drawing raster rows. One idea: When shrinkRatioY would be very high,
				// maintain current row position (don't draw this row) and extend out a tiny bit.
				const MAX_SHRINK_RATIO_Y = 1.01;
				if( shrinkRatioY > MAX_SHRINK_RATIO_Y ) {
					//console.log("Huge shrink ratio:", shrinkRatioY );
					shrinkRatioY = MAX_SHRINK_RATIO_Y;

					// Using shrinkRatioY: Determine the subpixel position where the row would be drawn.
					// This value helps reduce weird artifacts when the road goes over hills, etc (vanishing point is very
					// close to the row's Y, and this causes all kinds of weird jittery artifacts in the distance.)
					const SUB_PIXEL_MULT = 1.0 - (1.0 / MAX_SHRINK_RATIO_Y);
					var subY = SUB_PIXEL_MULT * (vanishingPoint.y - row);
					subPixelY += subY;

					if( subPixelY < 0.0 ) {
						subPixelY += 1.0;
						row = oldRow - 1;
					} else if( subPixelY >= 1.0 ) {
						subPixelY -= 1.0;
						row = oldRow + 1;
					} else {
						row = oldRow;	// don't draw this row. Advance Z and try drawing again next cycle.
					}
				}

				// We know how much the road should appear to shrink line-by-line. Multiply lastZ by that amount.
				var newZ = lastZ * shrinkRatioY;

				_dz = newZ - lastZ;

				var dzAdder = _dz * Math.max( 0.001, (1.0 - Math.abs( _dx * _dxReducesZ_rads )));
				_z += dzAdder;
			}

			roadPosition.z = _z;

			//y += (1.0 + dy);	// :( Oncoming uphill curves are really bad
			if( _z < -_horizonDistance ) {
				break;
			}

			// xOffset (horizontal alignment of this row texture): Head towards the vanishing point.
			xOffset = lerp( xOffset, vanishingPoint.x - render_width/2, shrinkRatioY-1.0 );

			// Roll. When the road is tilting, we're drawing a longer line, so expand the width of
			// the texture slice being drawn.
			var rowPivotY = -row + (render_height/2);// screen Y at the center of the road
			var diagonalHeight = render_width * roll_tan;	// Rolling: draw a diagonal with this height
			/*
			// This looks incorrect:
			var xOffsetRatioBi = xOffset / render_width;	// left=-0.5, right=0.5
			rowPivotY -= diagonalHeight * xOffsetRatioBi;	// shift the whole plane up/down, stay centered on road
			*/

			// Trying to center the middle of the road (U==0.5) and still allow planes to roll.
			var thisAngle = rollAngle - firstRollAngle;
			var thisRollTan = Math.tan( thisAngle );
			var thisRollCos = Math.cos( thisAngle );	// Because plane rotation changes the xOffset position on the screen.
			var offsetY = xOffset * -thisRollTan * 10 * thisRollCos;	// FIXME: Why is 10 the magic number here?

			// Roll changes the xOffset position, so I think I should multiply by roll_cos as well?
			// And, can this trig be reduced, condensce tan and cos to roll_sin?
			//offsetY += (xOffset*roll_sin);	// ?????

			rowPivotY += offsetY;	// center point of plane pivots on this .y location

			var drawY_L = (-diagonalHeight/2) + rowPivotY;
			var drawY_R = ( diagonalHeight/2) + rowPivotY;

			roadPosition.drawY_L = drawY_L;
			roadPosition.drawY_R = drawY_R;
			roadPosition.totalRollCos = roll_cos;
			roadPosition.totalRollTan = roll_tan;

			// To achieve correct positioning of Sprites in rows that _aren't_ on drawn planes:
			// Always compute & store roadPosition.centerX
			var centerX = ((xOffset * 0.01) * _z) * roll_cos_inv;
			roadPosition.centerX = centerX;

			if( (drawY_L > (render_height/2)) && (drawY_R > (render_height/2)) ) break;

			// courseResult has: .x (turn steepness), .y (hill), .z (roll), .splits (Array of [Vec2,Vec2]?)
			var courseResult = course.get_dx_y_roll_and_splits( (roadProgress-_z)*_scaleCourse );

			// dx values can change the angle at which we sample the texture
			var canDrawRow = (drawY_L >= (lastDrawnY_L-0.999)) || (drawY_R >= (lastDrawnY_R-0.999));	// TODO: space rows apart by at least 1px? Is this necessary?
			if( canDrawRow ) {
				// Roll: Change the positions of the row vertices
				var pos = groundQuadsDrawn * 12;
				groundPositions[pos+1] = drawY_L;	// top left
				groundPositions[pos+4] = lastDrawnY_L;	// bottom left
				groundPositions[pos+7] = drawY_R;	// top right
				groundPositions[pos+10] = lastDrawnY_R;	// bottom right

				// Set the Z coordinates for this row
				groundPositions[pos+2] = groundPositions[pos+5] = groundPositions[pos+8] = groundPositions[pos+11] = _z - 2.0;	// trying to fix z-fighting with road

				groundQuadsDrawn++;

				// Draw the road: Set positions at centerX, extend out
				var inv_z = 1.0 / _z;

				centerPt.set( ((centerX*inv_z)*1000), rowPivotY + 1 );	// Draw 1px above the ground :P

				// centerPt: rowPivotY is the plane's center pivot point.
				// So if the plane is perfectly flat, it's OK. But if there's roll, the actual
				// Y point is higher/lower.
				centerPt.y += roll_sin * centerPt.x;

				// Road width: If the plane has a roll angle, then we need to rotate
				// the road points around the center.
				var width = (-inv_z * _roadWidth);
				widthVector.set( width * roll_cos, width * roll_sin );

				var splits = courseResult.splits;
				for( var spl=0; spl<2; spl++ ) {
					var splitIdx = spl * 4;	// [posLeft,posRight,uvLeft,uvRight]

					leftPt.copy( widthVector ).multiplyScalar( splits[splitIdx] ).add( centerPt );
					rightPt.copy( widthVector ).multiplyScalar( splits[splitIdx+1] ).add( centerPt );
					var lastPts = lastPts_ar[ spl ];

					if( isFirstRow ) {
						lastPts[0].copy( leftPt ).setY( -999 );
						lastPts[1].copy( rightPt ).setY( -999 );
					}

					var roadPos = roadQuadsDrawn * 12;
					roadPositions[roadPos  ] = leftPt.x;
					roadPositions[roadPos+1] = leftPt.y;
					roadPositions[roadPos+3] = lastPts[0].x;
					roadPositions[roadPos+4] = lastPts[0].y;

					roadPositions[roadPos+6] = rightPt.x;
					roadPositions[roadPos+7] = rightPt.y;
					roadPositions[roadPos+9] = lastPts[1].x;
					roadPositions[roadPos+10] = lastPts[1].y;

					// Set the Z coordinates for this row
					roadPositions[roadPos+2] = roadPositions[roadPos+5] = roadPositions[roadPos+8] = roadPositions[roadPos+11] = _z;

					// Store for the next row
					lastPts[0].copy( leftPt );
					lastPts[1].copy( rightPt );

					// Road texture UV
					roadLeftUV.x = splits[splitIdx+2];
					roadRightUV.x = splits[splitIdx+3];
					roadLeftUV.y = roadRightUV.y = (txPhase - _z) * _txScaleY;
					var lastUVs = roadLastUVs_ar[ spl ];

					var uvIdx = roadQuadsDrawn * 8;
					roadUVs[uvIdx  ] = roadLeftUV.x;
					roadUVs[uvIdx+1] = roadLeftUV.y;
					roadUVs[uvIdx+4] = roadRightUV.x;
					roadUVs[uvIdx+5] = roadRightUV.y;

					roadUVs[uvIdx+2] = lastUVs[0].x;
					roadUVs[uvIdx+3] = lastUVs[0].y;
					roadUVs[uvIdx+6] = lastUVs[1].x;
					roadUVs[uvIdx+7] = lastUVs[1].y;

					lastUVs[0].copy( roadLeftUV );
					lastUVs[1].copy( roadRightUV );

					roadQuadsDrawn++;

					if( spl===0 ) lastDrawnY_L = drawY_L;//Math.max( lastDrawnY_L, drawY_L );
					if( spl===1 ) lastDrawnY_R = drawY_R;//Math.max( lastDrawnY_R, drawY_R );
				}

			}	// end if( canDrawRow )

			var curveX = courseResult.x * _turnSteep;
			_dx += (curveX * 0.005) * _dz * _scaleCourse;

			var curveY = courseResult.y * _hillSteep;
			if( isFirstRow ) {

			} else {
				//dy += ((curveY-lastCurveY) * 0.1) * _hillSteep;	// No, we want to control dy based on fixed y values
				_y = curveY;
				_dy = curveY - lastCurveY;	// close, but not quite! Needs to account for differences in z.

				const GLOBAL_HILL_STEEPNESS = -0.05;
				_dy *= (GLOBAL_HILL_STEEPNESS/_dz);

				//_dy *= roll_cos;	// no, I think this is incorrect..... roll shouldn't affect _dy.
			}
			lastCurveY = curveY;

			_roll = courseResult.z * _rollSteep;

			if( isFirstRow ) {
				// Pan the background
				var panX = curveX * (drive_speed/59.0) * timeMult * _turnSteep;
				var panY = first_dy * _hillSteep/5;

				bg_U -= panX * (_bgScrollX*0.001);
				bg_V = 0.25 - panY * _bgScrollY	;//((i-(render_height/2)) / (render_height/2)) * _bgScrollY;

				// Keyboard driving: Curves should throw you off the road
				if( isKeyboardDriving ) {
					drive_xOffset -= drive_speed * curveX * settings.get(KEYBOARD_HARSH_CURVES) * (1.0/1000);
					drive_xOffset -= drive_speed * firstRoll_sin * settings.get(KEYBOARD_HARSH_ROLL) * (1.0/220);
				}

				// Scroll the octave textures
				function scrollOctaveTextures( shader, adder, prefix, txScale, strafeAmt ){
					adder.x -= panX * settings.get(prefix+OCTAVE_SCROLL_X) * 0.001;
					// Smooth out the hill movement, try to eliminate chunky climb/descend texture artifacts
					adder.y -= (panY - lastPanY) * settings.get(prefix+OCTAVE_SCROLL_Y) * 0.1;
					adder.z = -firstRollAngle * settings.get(prefix+OCTAVE_ROLL);

					shader.uniforms['octaveScroll'].value.set(
						adder.x - drive_xOffset * txScale * strafeAmt,
						adder.y,
						adder.z
					);
				}
				scrollOctaveTextures( groundShader, groundOctaveScrollAdder, 'G:', _groundTxScale, _groundStrafeAmt );
				scrollOctaveTextures( roadShader, roadOctaveScrollAdder, 'R:', _roadTxScale, _roadStrafeAmt );

				moonShader.uniforms['travelOffset'].value += timeMult * settings.get('M:'+TRAVEL) * 0.1;
				moonShader.uniforms['colorLoops'].value = settings.get('M:'+COLOR_LOOPS);

				spriteShader.uniforms['travelOffset'].value += timeMult * settings.get('S:'+TRAVEL) * 0.1;
				spriteShader.uniforms['colorLoops'].value = settings.get('S:'+COLOR_LOOPS);

				fireShader.uniforms['travelOffset'].value += timeMult * settings.get('F:'+TRAVEL) * 0.1;
				fireShader.uniforms['colorLoops'].value = settings.get('F:'+COLOR_LOOPS);
				fireShader.uniforms['txScale'].value = _fireTxScale;
				fireShader.uniforms['txScroll'].value.x += settings.get('F:'+TX_SCROLL_X) * _fireTxScale * timeMult;
				fireShader.uniforms['txScroll'].value.y -= settings.get('F:'+TX_SCROLL_Y) * _fireTxScale * timeMult;
				fireShader.uniforms['txHardEdge'].value = settings.get('F:'+TX_HARD_EDGE);
				fireShader.uniforms['ripplePhase'].value -= timeMult * settings.get('F:'+RIPPLE_SPEED);
				fireShader.uniforms['rippleFreq'].value = settings.get('F:'+RIPPLE_FREQ);
				fireShader.uniforms['rippleStrength'].value = settings.get('F:'+RIPPLE_STRENGTH);

				headlightShader.uniforms['travelOffset'].value += timeMult * settings.get('H:'+TRAVEL) * 0.1;
				headlightShader.uniforms['txScale'].value = _headlightTxScale;
				headlightShader.uniforms['headlightScale'].value = _headlightScale;
				headlightShader.uniforms['distanceY'].value = settings.get('H:'+DISTANCE_Y);
				headlightShader.uniforms['distanceScale'].value = settings.get('H:'+DISTANCE_SCALE) * 0.0001;
				headlightShader.uniforms['ditherTunnel'].value = settings.get('H:'+DITHER_TUNNEL);
				const STEERING_AMT_X = 0.04;
				const STEERING_AMT_Y = 0.01;
				headlightShader.uniforms['steering'].value.lerp( new THREE.Vector2( keyDirX*STEERING_AMT_X, keyDirY*STEERING_AMT_Y ), 0.03*timeMult );

				for( var i=0, len=bgSprites.length; i<len; i++ ) {
					bgSprites[i].update( panX, panY, roadDelta, roadPositionAr, firstRollAngle, timeMult );
				}

				lastPanY = panY;

			}

			isFirstRow = false;

			lastZ = _z;
		}

		// Heading uphill: Sometimes lastDrawnRow doesn't reach 0. Make sure we hide subsequent rows.
		groundGeom.setDrawRange(0, groundQuadsDrawn * 6);
		roadGeom.setDrawRange(0, roadQuadsDrawn * 6);

		groundGeom.attributes.position.needsUpdate = true;	// rows can move because of roll
		roadGeom.attributes.position.needsUpdate = true;	// rows can move because of roll
		roadGeom.attributes.uv.needsUpdate = true;

		// Update shaders
		var addGroundOctave = roadDelta * settings.get('G:'+OCTAVE_SPEED) / settings.get('G:'+OCTAVE_SIZE);
		groundOctave = wrap( groundOctave + addGroundOctave, 1.0 );
		var addRoadOctave = roadDelta * settings.get('R:'+OCTAVE_SPEED) / settings.get('R:'+OCTAVE_SIZE);
		roadOctave = wrap( roadOctave + addRoadOctave, 1.0 );

		function updateShader( shader, propPrefix, txScale, travelOffset, octave ){
			shader.uniforms['colorLoops'].value = settings.get( propPrefix + 'color_loops' );
			shader.uniforms['overlap'].value = settings.get( propPrefix + 'overlap' );
			shader.uniforms['txScale'].value = txScale;
			shader.uniforms['travelOffset'].value = travelOffset;
			shader.uniforms['depthCurve'].value = settings.get( propPrefix + 'depth_curve' );

			var _octaveSpeed = settings.get( propPrefix+OCTAVE_SPEED );
			var _octaveSize = settings.get( propPrefix+OCTAVE_SIZE );

			shader.uniforms['octaveSize0'].value = Math.pow( _octaveSize, -octave );
			shader.uniforms['octaveAlpha0'].value = 1.0 - octave;
			shader.uniforms['octaveSize1'].value = Math.pow( _octaveSize, 1.0-octave );
			shader.uniforms['octaveAlpha1'].value = octave;
			shader.uniforms['skewMult'].value = settings.get( propPrefix + 'skew_mult' );
			shader.uniforms['farCrunch'].value.set( settings.get(propPrefix+FAR_CRUNCH_X), settings.get(propPrefix+FAR_CRUNCH_Y) );
		}
		updateShader( groundShader, 'G:', _groundTxScale, groundTravelOffset, groundOctave );
		updateShader( roadShader, 'R:', _roadTxScale, roadTravelOffset, roadOctave );

		groundTravelOffset -= roadDelta * settings.get( 'G:'+TRAVEL );
		roadTravelOffset -= roadDelta * settings.get( 'R:'+TRAVEL );

		var bgU_center = bg_U;
		const BG_WIDTH_2 = 0.75;//250 / 512;
		var bgV_center = bg_V;
		const BG_HEIGHT_2 = 0.75;//250 / 512;

		var bgRollAngle = -firstRollAngle * settings.get(BG_ROLL);
		var p0 = new THREE.Vector2( BG_WIDTH_2, BG_HEIGHT_2 );
		rotateVec2( p0, bgRollAngle );
		var p1 = new THREE.Vector2( -BG_WIDTH_2, BG_HEIGHT_2 );
		rotateVec2( p1, bgRollAngle );

		//   0---2  2
		//   |#0/  /|
		//   | /  / |
		//   |/  /#1|
		//   1  0---1

		var bgUVs = bg.geometry.faceVertexUvs[0];

		bgUVs[0][0].x = p1.x + bgU_center;
		bgUVs[0][0].y = p1.y + bgV_center;
		bgUVs[0][1].x = -p0.x + bgU_center;
		bgUVs[0][1].y = -p0.y + bgV_center;
		bgUVs[0][2].x = p0.x + bgU_center;
		bgUVs[0][2].y = p0.y + bgV_center;

		bgUVs[1][0].x = -p0.x + bgU_center;
		bgUVs[1][0].y = -p0.y + bgV_center;
		bgUVs[1][1].x = -p1.x + bgU_center;
		bgUVs[1][1].y = -p1.y + bgV_center;
		bgUVs[1][2].x = p0.x + bgU_center;
		bgUVs[1][2].y = p0.y + bgV_center;

		bg.geometry.uvsNeedUpdate = true;

		// Sort by Z, this will determine the draw order
		sprites.sort( function(a,b){ return (a.loc.z - b.loc.z) } );

		var spritesDrawn = 0;
		var headlightsDrawn = 0;
		var firesDrawn = 0;
		var effects = [];
		for( var i=0, len=sprites.length; i<len; i++ ) {
			var spr = sprites[i];

			if( spr.effect==='headlight' ) {
				var result = spr.update( headlightsDrawn, headlightPositions, headlightUVs, -camera.far, roadPositionAr, firstRollAngle, settings.get(DRAW_PRECISION) );
				if( result ) {
					headlightsDrawn++;
				}

			} else {
				var result = spr.update( spritesDrawn, spritePositions, spriteUVs, -camera.far, roadPositionAr, firstRollAngle, settings.get(DRAW_PRECISION) );
				if( result ) {
					effects.push( spr.effect );
					spritesDrawn++;
				}
			}
		}
		spriteGeom.attributes.position.needsUpdate = true;
		spriteGeom.attributes.uv.needsUpdate = true;
		spriteGeom.setDrawRange(0, spritesDrawn * 6);

		headlightGeom.attributes.position.needsUpdate = true;
		headlightGeom.attributes.uv.needsUpdate = true;
		headlightGeom.setDrawRange(0, headlightsDrawn * 6);

		// Some sprites have 'balefire' effect. Prepare quads for BalefireShader.
		for( var i=0; i<spritesDrawn; i++ ) {
			// Leech the Sprite location from spritePositions
			var pos = i * 4 * 3;
			var root = new THREE.Vector2(
				(spritePositions[pos+6] + spritePositions[pos+9]) / 2,
				(spritePositions[pos+7] + spritePositions[pos+10]) / 2
			);

			if( effects[i]==='balefire' ) {
				const FIRE_SCALE = 2.0;
				var fPos = firesDrawn * 4 * 3;
				for( var j=0; j<12; j+=3 ) {
					// stretch X and Y
					firePositions[fPos+j  ] = root.x + (spritePositions[pos+j  ]-root.x) * FIRE_SCALE;
					firePositions[fPos+j+1] = root.y + (spritePositions[pos+j+1]-root.y) * FIRE_SCALE;
					firePositions[fPos+j+2] = spritePositions[pos+j+2] - 1.0;	// copy Z, behind sprite tho
				}
				firesDrawn++;
			}
		}

		fireGeom.attributes.position.needsUpdate = true;
		//fireGeom.attributes.uv.needsUpdate = true;
		fireGeom.setDrawRange(0, firesDrawn * 6);

		if( is1980s ) {
			for( var i=0; i<roadPositions.length; i+=3 ) {
				roadPositions[i  ] = Math.round( roadPositions[i  ] * 0.25 ) * 4.0;	// x
				roadPositions[i+1] = Math.round( roadPositions[i+1] * 0.25 ) * 4.0;	// y
			}
		} else if( is1990s ) {
			for( var i=0; i<roadPositions.length; i+=3 ) {
				roadPositions[i  ] = Math.round( roadPositions[i  ] );	// x
				roadPositions[i+1] = Math.round( roadPositions[i+1] );	// y
			}
		}

		// Draw to screen!
		if (renderer) {
			renderer.render(gl_scene, camera);
		}

		if( stats ) {
			stats.update();
		}

	}	// !self.perFrame()

}
