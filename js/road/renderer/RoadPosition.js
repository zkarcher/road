// RoadPosition
//
//    Used by RoadRenderer, during raster computation

function RoadPosition(){
	var self = this;

	self.z = 0;
	self.drawY_L = 0;
	self.drawY_R = 0;
	self.centerX = 0;
	self.totalRollCos = 1.0;
	self.totalRollTan = 0.0;
}
