console.log("Hello world! Vroooooooom");

var Road = function(){
	var self = this;

	runSplineTest();

	console.log("Road [CTOR]");

	var view = new RoadView();
	self.getView = function(){ return view; }

	var prevMS = 0;
	var bg;

	var course;	// Course the player is driving on

	//self.dump = function(){ console.log( roadUVs ) };

	// Controls: Speed & steering
	var drive_speed = DEFAULT_DRIVE_SPEED;
	self.getDriveSpeed = function(){ return drive_speed; }
	var drive_xOffset = 0.0;
	var drive_dx = 0.0;
	var isKeyboardDriving = false;
	self.getIsKeyboardDriving = function(){ return isKeyboardDriving; }

	var keyDirX=0, keyDirY=0;
	self.getKeyDirX = function(){ return keyDirX; }
	self.getKeyDirY = function(){ return keyDirY; }

	// Battery saver: Don't run in the background
	var isFocused = true;
	window.onfocus = function(){ isFocused = true };
	window.onblur = function(){ isFocused = false };
	window.focus();

	var settings = new RoadSettings(self);
	var renderer = new RoadRenderer(self, settings);

	function updateCurveType(){
		function getCurveFuncByTitle( title ) {
			switch( title ) {
				case 'bezier':    return interpolateBezier;
				case 'linear':    return interpolateLinear;
				case 'spline':    return interpolateSpline;
				case 'windowed':  return interpolateWindowed;
				default:          console.log("updateCurveType: Cannot interpret this type:", title );
			}
		}
		dxCurveFunc = getCurveFuncByTitle( settings.get(DX_CURVE_TYPE) );
		yCurveFunc = getCurveFuncByTitle( settings.get(Y_CURVE_TYPE) );
		rollCurveFunc = getCurveFuncByTitle( settings.get(ROLL_CURVE_TYPE) );
	}

	function onWindowResize() {
		w = Math.min(MAX_RENDER_WIDTH, window.innerWidth);
		h = Math.min(MAX_RENDER_HEIGHT, window.innerHeight);

		if (renderer.isReady()) {
			var camera = renderer.getCamera();
			camera.left = -w/2;
			camera.right = w/2;
			camera.top = h/2;
			camera.bottom = -h/2;
			camera.near = 0;
			camera.far = settings.get(HORIZON_DISTANCE);
			camera.updateProjectionMatrix();

			//renderer.setSize( window.innerWidth, window.innerHeight );
			renderer.setSize( w, h );
		}

		renderer.onWindowResize();

		if( bg ) bg.position.z = -settings.get(HORIZON_DISTANCE);

		for( var i=0; i<view.bgSprites.length; i++ ) {
			view.bgSprites[i].loc.z = -settings.get(HORIZON_DISTANCE);
		}

		// Center the WebGL canvas in the window
		var canvas = document.getElementById("threeJS_canvas");
		canvas.style.position = "fixed";
		canvas.style.left = ((window.innerWidth - w) / 2) + "px";
		canvas.style.top = ((window.innerHeight - h) / 2) + "px";
	}

	self.updateDitherTexture = function( prefix ){
		renderer.updateDitherTexture(prefix);
	}

	self.updateHeadlightTexture = function( prefix ) {
		renderer.updateHeadlightTexture(prefix);
	}

	self.updatePalette = function( prefix, doReroll ){
		renderer.updatePalette(prefix, doReroll);
	}

	function resetCourse(){
		// Remove sprites
		if (course) {
			course.destroy();
		}

		if (view) {
			view.reset();
			view.bgSprites.push(renderer.getMoon());
		}

		roadPosition = 0.0;
		course = null;
	}

	self.setCourse = function( createCourse_func ) {
		resetCourse();

		course = createCourse_func();

		console.log("Starting course ::", course['title']);
	}

	//
	//  INPUT
	//
	self.setSliderXOffset = function(){
		drive_xOffset = settings.get(X_OFFSET);
		isKeyboardDriving = false;
	}

	self.setSliderdrive_speed = function(){
		drive_speed = settings.get(DRIVE_SPEED);
		isKeyboardDriving = false;
	}

	self.setHorizonDistance = function(){
		onWindowResize();
	}

	self.setCurveChange = function(){
		updateCurveType();
	}

	document.onkeydown = function(evt){
		//console.log("down", evt.keyCode);
		if( (37<=evt.keyCode) && (evt.keyCode<=40) ) {
			isKeyboardDriving = true;

			switch( evt.keyCode ) {
				case 37:	keyDirX = -1; break;	// left
				case 39:	keyDirX = 1; break;	// right
				case 38:	keyDirY = 1; break;	// up
				case 40:	keyDirY = -1; break;	// down
			}
		}
	}
	document.onkeyup = function(evt){
		//console.log("up", evt.keyCode);
		switch( evt.keyCode ) {
			case 37:	if( keyDirX === -1) { keyDirX = 0; break; }	// left
			case 39:	if( keyDirX === 1) { keyDirX = 0; break; }	// right
			case 38:	if( keyDirY === 1 ) { keyDirY = 0; break; }	// up
			case 40:	if( keyDirY === -1 ) { keyDirY = 0; break; }	// down
		}
	}

	function perFrame() {
		requestAnimationFrame( perFrame );

		if (!renderer.isReady()) return;

		// After losing focus: Don't run in the background
		if( !isFocused && settings.get(BATTERY_SAVER) ) return;

		if( !course ) return;	// sanity check

		// Frame rate may not be constant 60fps. Time between frames determines how
		// quickly to advance animations.
		var ms = new Date();
		var elapsed = (ms - prevMS) * 0.001;	// ms to seconds
		var timeMult = elapsed * 60.0;			// Animations are cooked at 60fps :P
		timeMult = Math.min( 4.0, timeMult );	// Prevent grievous skipping
		prevMS = ms;

		if( isKeyboardDriving ) {
			if( keyDirX===0.0 ) {
				drive_dx *= Math.pow( settings.get(KEYBOARD_STEERING_DECEL), timeMult );	// converge towards zero

			} else {
				var _maxSteer_dx = settings.get(KEYBOARD_MAX_STEER_DX);
				var _tightSteering = settings.get(KEYBOARD_TIGHT_STEERING);	// 0.0==no steering. 1.0 and higher==very tight.
				drive_dx = clamp( drive_dx - keyDirX*_tightSteering*timeMult, -_maxSteer_dx, _maxSteer_dx );
			}
			drive_xOffset = clamp( drive_xOffset + drive_dx, -MAX_DRIVE_STRAFE, MAX_DRIVE_STRAFE );
			drive_speed = clamp( drive_speed + keyDirY*2.0, -MAX_DRIVE_SPEED, MAX_DRIVE_SPEED );
		}

		var roadDelta = drive_speed * 0.001 * timeMult;

		renderer.perFrame(timeMult, course, roadDelta, drive_xOffset);
	}

	window.addEventListener( 'resize', onWindowResize, false );

	// Create scene & camera
	settings.initGUI();

	renderer.init3D().then(
		function(){
			self.setCurveChange();

			onWindowResize();

			self.setCourse( createCourse_randomFun );
			//setCourse( createCourse_testFlat );

			// start render loop
			perFrame();
		}
	);
}

var _road;
window.addEventListener("DOMContentLoaded", function(){
 	_road = new Road();
}, false );
