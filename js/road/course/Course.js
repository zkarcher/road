var dxCurveFunc, yCurveFunc, rollCurveFunc;

function Course( inTitle ){
	var self = this;
	self.title = inTitle;

	var segments = [];	// of CourseSegment's
	self.getSegments = function() {
		return segments;
	}

	var sprites = [];	// of Sprite's
	self.getSprites = function() {
		return sprites;
	}

	self.initWithDxDy_array = function( ar, in_splits ) {
		var atY = 0;
		for( var i=0, len=ar.length; i<len; i++ ) {
			var obj = ar[i];

			var splitAmt = in_splits ? in_splits[i] : 0;

			var spl_ar = [0,0,0,0, 0,0,0,0];
			// Welcome to the left-hand path, my friend
			spl_ar[0] = -1 - splitAmt;	// left position
			spl_ar[1] = Math.min( 0, spl_ar[0]+2.0 );	// right position
			spl_ar[2] = 0;	// left UV
			spl_ar[3] = (spl_ar[1] - spl_ar[0]) * 0.5;	// right UV

			// Um there is also a right-hand path
			spl_ar[5] = 1 + splitAmt;	// right position
			spl_ar[4] = Math.max( 0, spl_ar[5]-2.0 );	// left position
			spl_ar[7] = 1.0;	// right UV
			spl_ar[6] = Math.max( 0, (2.0 - spl_ar[5]) * 0.5 );	// left UV

			var seg = new CourseSegment( obj.x, atY, obj.z, new Float32Array(spl_ar) );
			segments.push( seg );

			atY += obj.y;
		}

		// Also create quadratic Bezier control points between end points.
		// Draw lines from previous point to this one, and a line from the next two segments. If they
		// intersect, then this will make a nice control point, for a seamless Bezier curve.
		var len = segments.length;
		for( var i=0; i<len; i++ ) {
			var seg0 = segments[ wrap( i-1, len ) ];
			var seg1 = segments[ wrap( i  , len ) ];
			var seg2 = segments[ wrap( i+1, len ) ];
			var seg3 = segments[ wrap( i+2, len ) ];

			// Solve lines in general equation form: Ax + By = C
			// Need to infer this from two points. Thanks, stackoverflow:
			//   a = y1-y2,
			//   b = x2-x1,
			//   c = (x1-x2)*y1 + (y2-y1)*x1

			var A1 = seg0.y - seg1.y;
			var B1 = seg1.dx - seg0.dx;
			var C1 = (seg0.dx - seg1.dx) * seg0.y + (seg1.y - seg0.y) * seg0.dx;

			var A2 = seg2.y - seg3.y;
			var B2 = seg3.dx - seg2.dx;
			var C2 = (seg2.dx - seg3.dx) * seg2.y + (seg3.y - seg2.y) * seg2.dx;

			var delta = A1 * B2 - A2 * B1;
			if( delta === 0 ) {
				// Lines are parallel
			} else {
				seg1.bezierCtrlX = ( B2 * C1 - B1 * C2 ) / delta;
				seg1.bezierCtrlY = ( A1 * C2 - A2 * C1 ) / delta;
			}
		}

		return self;
	}

	self.addRandomSprites = function() {
		/*
		switch( DEMO ) {
			case 'normal':
				// Ohhhh it is the graveyard. Miserere
				for( var i=0; i<Math.min(256,MAX_SPRITES); i++ ) {
					var spriteKey = randFromArray( atlasKeys );
					if( !spriteKey ) continue;	// wtf? sometimes this is undefined

					var atX = randRangeBi( 0.5, 3.0 );
					var loc = new THREE.Vector3( atX, 0, randRange(-100,0) );
					var sprite = new Sprite( 'balefire', randRange(0.7,0.9) * atlasScales[spriteKey], loc, spriteAtlas[spriteKey] );
					sprites.push( sprite );
				}
				break;

			case 'headlight':
				// You are in the woods, sorta kinda
				var maxTreeSprites = 1 + BRANCH_COUNT_MAX;
				var treeCount = Math.floor(MAX_SPRITES/maxTreeSprites);
				treeCount = Math.min( treeCount, 35 );
				for( var i=0; i<treeCount; i++ ) {
					var atX = randRangeBi( 0.5, 3.0 );
					var loc = new THREE.Vector3( atX, 0, randRange(-100,0) );
					var group = new SpriteGroup( 1.0, loc, 'tree', spriteAtlas );
					view.spriteGroups.push( group );
					view.sprites = view.sprites.concat( group.sprites );
				}
				break;
		}
		*/

		var segmentsLen = segments.length;
		for( var i=0; i<Math.min(256,MAX_SPRITES); i++ ) {
			var spriteKey = randFromArray( atlasKeys );

			// RoadSettings: Builds courses to get their titles, before sprites have init'd
			if (!spriteAtlas[spriteKey]) continue;

			var atX = randRangeBi( 0.5, 3.0 );
			var loc = new THREE.Vector3( atX, 0, randRange(-segmentsLen,0) );
			var sprite = new Sprite( 'balefire', randRange(0.7,0.9) * atlasScales[spriteKey], loc, spriteAtlas[spriteKey] );
			sprites.push( sprite );
		}

		return self;
	}

	self.get_dx_y_roll_and_splits = function( courseZ ) {
		// Rows that are further away: Interpolate the road and take multiple samples
		// to reduce wiggly artifacts in the distance
		/*
		var multisampleCurves = Math.floor( Math.max( 1.0, Math.log( courseZ - cameraZ ) ) ) * 2;
		var multisample_i = 1.0 / multisampleCurves;

		var curveX_sum=0, curveY_sum=0;
		for( var c=1; c<=multisampleCurves; c++ ) {
			var progress = c * multisample_i;

			var sampleZ = lerp( lastCourseZ, courseZ, progress );
			var floorSeg = segments[ wrap( Math.floor(sampleZ), segments.length ) ];
			var ceilSeg = segments[ wrap( Math.ceil(sampleZ), segments.length ) ];
			var lerpRatio = sampleZ - Math.floor(sampleZ);

			curveX_sum += lerp( floorSeg.dx, ceilSeg.dx, lerpRatio );
			curveY_sum += lerp( floorSeg.y, ceilSeg.y, lerpRatio );
		}

		return new THREE.Vector2(
			curveX_sum * multisample_i,
			curveY_sum * multisample_i
		);
		*/

		var floorIdx = Math.floor( courseZ );
		var seg0 = segments[ wrap( floorIdx-1, segments.length ) ];
		var seg1 = segments[ wrap( floorIdx  , segments.length ) ];
		var seg2 = segments[ wrap( floorIdx+1, segments.length ) ];
		var seg3 = segments[ wrap( floorIdx+2, segments.length ) ];

		var t = courseZ - floorIdx;

		return {
			x:   dxCurveFunc( seg0.dx,   seg1.dx,   seg2.dx,   seg3.dx,   t,   seg1.bezierCtrlX ),
			y:    yCurveFunc( seg0.y,    seg1.y,    seg2.y,    seg3.y,    t,   seg1.bezierCtrlY ),
			z: rollCurveFunc( seg0.roll, seg1.roll, seg2.roll, seg3.roll, t,   seg1.roll ),	// FIXME: bezier ctrl value?
			splits: interpolateSplits( seg0.splits, seg1.splits, seg2.splits, seg3.splits, t ),
		};
	}

	self.perFrame = function(timeMult, roadProgress, roadDelta) {
		for( var i = 0; i < sprites.length; i++ ) {
			sprites[i].updateDZ( roadDelta );
		}
	}

	self.destroy = function() {
		for(var i = 0; i < sprites.length; i++) {
			sprites[i].destroy();
		}
	}
}

function interpolateBezier( p0, p1, p2, p3, t, ctrl ) {
	// lerp p1 to ctrl pt
	var s1 = lerp( p1, ctrl, t );
	var s2 = lerp( ctrl, p2, t );

	return lerp( s1, s2, t );
}

function interpolateLinear( p0, p1, p2, p3, t ) {
	return lerp( p1, p2, t );
}

// Catmull-Rom spline interpolation
function interpolateSpline( p0, p1, p2, p3, t ) {
	var t2 = t * t;
	var t3 = t2 * t;

	var v0 = ( p2 - p0 ) * 0.5;
	var v1 = ( p3 - p1 ) * 0.5;

	var result = (2*(p1-p2) + v0 + v1) * t3;
	result += (-3*(p1-p2) - 2*v0 - v1) * t2;
	result += v0*t;
	result += p1;
	return result;
};

// Moving window function. (Raised cosine)
// This will average out the values and hopefully provide smooth curves
// with no protrusions.
const PI_2 = Math.PI/2;
var windowAr = new Float32Array( 4 );
function interpolateWindowed( p0, p1, p2, p3, t ) {
	/*
	// Raised triangle window (this creates flat linear segments, feels bad)
	windowAr[0] = 1.0 - t;	// 1..0
	windowAr[1] = 2.0 - t;	// 2..1
	windowAr[2] = t + 1.0;	// 1..2
	windowAr[3] = t;	// 0..1
	*/

	// Raised cosine window, height of 2.0
	// Creates slopes with bouncy bumps, hmm.
	windowAr[0] = Math.cos( (t+1) * PI_2 ) + 1.0;
	windowAr[1] = Math.cos(  t    * PI_2 ) + 1.0;
	windowAr[2] = Math.cos( (t-1) * PI_2 ) + 1.0;
	windowAr[3] = Math.cos( (t-2) * PI_2 ) + 1.0;

	// Squaring the weights helps remove bumpy influence from neighboring bins,
	// but not entirely.
	for( var i=0; i<4; i++ ) {
		windowAr[i] *= windowAr[i];
	}

	var sum = windowAr[0] + windowAr[1] + windowAr[2] + windowAr[3];
	var sum_i = 1.0 / sum;

	return windowAr[0]*sum_i*p0 + windowAr[1]*sum_i*p1 + windowAr[2]*sum_i*p2 + windowAr[3]*sum_i*p3;
}

// each split is: [posLeft,posRight,uvLeft,uvRight]
var _splits_fast = new Float32Array( 8 );
function interpolateSplits( splits0, splits1, splits2, splits3, t ) {
	for( var i=0; i<8; i++ ) {
		_splits_fast[i] = interpolateSpline( splits0[i], splits1[i], splits2[i], splits3[i], t );
	}
	return _splits_fast;
}

function runSplineTest(){
	var expected = [0, 0.07582990397805213, 0.17651577503429355, 0.29518518518518516, 0.42496570644718795, 0.5589849108367627, 0.6903703703703703, 0.8122496570644718, 0.9177503429355282, 1];
	var actual = [];
	var passed = true;
	for( var i=0; i<10; i++ ) {
		var t = i/9;
		var t2 = t*t;
		var t3 = t*t*t;
		actual.push( interpolateSpline( -0.1, 0, 1, 1.23, t, t2, t3 ) );
		if( actual[i] !== expected[i] ) passed = false;
	}

	if( passed ) {
		console.log( "Spline test: passed");
	} else {
		console.warn( "** Spline test: FAILED ::", actual );
	}
}
