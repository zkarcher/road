function createCourse_randomFun(){
	var curves = [];
	var splits = [];

	const STEPS = 100;
	var xs = [];
	var ys = [];
	var rolls = [];
	for( var i=0; i<STEPS; i++ ) {
		const MAX_X = 0.2;
		xs.push( randRange(-MAX_X,MAX_X) );
		const MAX_Y = 0.7;
		ys.push( randRange(-MAX_Y,MAX_Y) );
		const MAX_ROLL = 0.5;
		rolls.push( randRange(-MAX_ROLL,MAX_ROLL) );
	}

	function sineInOut(f){
		return (Math.cos(f*Math.PI) - 1.0) * -0.5;
	}

	for( var i=0; i<STEPS; i++ ) {
		// Length of this section
		var len = Math.round( randRange( 5, 12 ) );

		// Lerp between the old x/y/roll, and the new one
		var fromVec = new THREE.Vector3( xs[wrap(i,STEPS)], ys[wrap(i,STEPS)], rolls[wrap(i,STEPS)] );
		var toVec = new THREE.Vector3( xs[wrap(i+1,STEPS)], ys[wrap(i+1,STEPS)], rolls[wrap(i+1,STEPS)] );

		for( var j=0; j<len; j++ ) {
			curves.push( new THREE.Vector3().lerpVectors( fromVec, toVec, sineInOut(j/len) ) );
		}
	}

	// Let's make some rando splits too
	var splitAmt = 0;
	var splitDir = 0;
	for( var i=0; i<curves.length; i++ ) {
		if( (i===0) || (rand()<0.2) ) {
			splitDir = randBi(MAX_ROAD_SPLIT_AMT / 10.0);
		}

		splitAmt += splitDir;
		if( splitAmt < 0 ) {
			splitAmt = 0;
			splitDir = Math.abs( splitDir );

		} else if( splitAmt > MAX_ROAD_SPLIT_AMT ) {
			splitAmt = MAX_ROAD_SPLIT_AMT;
			splitDir = -Math.abs( splitDir );
		}

		/*
		var ar = [0,0,0,0, 0,0,0,0];
		// Welcome to the left-hand path, my friend
		ar[0] = -1 - splitAmt;	// left position
		ar[1] = Math.min( 0, ar[0]+2.0 );	// right position
		ar[2] = 0;	// left UV
		ar[3] = (ar[1]-ar[0]) * 0.5;	// right UV

		// Um there is also a right-hand path
		ar[5] = 1 + splitAmt;	// right position
		ar[4] = Math.max( 0, ar[5]-2.0 );	// left position
		ar[7] = 1.0;	// right UV
		ar[6] = Math.max( 0, (2.0-ar[5]) * 0.5 );	// left UV

		splits.push( new Float32Array( ar ) );
		*/

		splits.push(splitAmt);
	}

	return new Course("Random Fun").initWithDxDy_array( curves, splits ).addRandomSprites();
}

function createCourse_testFlat(){
	var curves = [];

	// Smooth left & right
	var CURVE_SPAN = 8;
	const SOFT_CURVE = 0.1;
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( (i/CURVE_SPAN) * SOFT_CURVE, 0 ) ) };
	for( var i=CURVE_SPAN-1; i>=0; i-- ) { curves.push( new THREE.Vector2( (i/CURVE_SPAN) * SOFT_CURVE, 0 ) ) };
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( (-i/CURVE_SPAN) * SOFT_CURVE, 0 ) ) };
	for( var i=CURVE_SPAN-1; i>=0; i-- ) { curves.push( new THREE.Vector2( (-i/CURVE_SPAN) * SOFT_CURVE, 0 ) ) };

	// Hard left & right
	var CURVE_SPAN = 24;
	const HARD_CURVE = 0.5;
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( HARD_CURVE, 0 ) );
	for( var i=0; i<CURVE_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( -HARD_CURVE, 0 ) );

	return new Course("Test Flat").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_tightTurns() {
	var curves = [];

	var STRAIGHT_SPAN = 3;
	const HARD_CURVE = 0.5;
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( HARD_CURVE, 0 ) );
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( -HARD_CURVE, 0 ) );
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };

	const HARDER_CURVE = 1.0;
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( HARDER_CURVE, 0 ) );
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	curves.push( new THREE.Vector2( -HARDER_CURVE, 0 ) );
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };

	return new Course("Tight Turns").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_hillTest(){
	var curves = [];

	var STRAIGHT_SPAN = 8;
	var HILL_SPAN = 4;
	var HILL_AMT = 1.0;
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	for( var i=0; i<HILL_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, -HILL_AMT ) ) };
	for( var i=0; i<STRAIGHT_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
	for( var i=0; i<HILL_SPAN; i++ ) { curves.push( new THREE.Vector2( 0, HILL_AMT ) ) };

	return new Course("Hill Test").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_hillTestValleys(){
	var curves = [];

	var SPAN = 10;
	var HILL_AMT = 1.0;
	for( var i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, -HILL_AMT ) ) };
	for( var i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, HILL_AMT ) ) };

	return new Course("Valleys").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_hillTestLongValleys(){
	var curves = [];

	var SPAN = 20;
	var HILL_AMT = 1.0;
	for( var i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, -HILL_AMT ) ) };
	for( var i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, HILL_AMT ) ) };

	return new Course("Valleys Long").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_hillTestRollingHills(){
	var curves = [];

	var SPAN = 10;
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, i/SPAN ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, (SPAN-i)/SPAN ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, -i/SPAN ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, -(SPAN-i)/SPAN ) ) };

	return new Course("Rolling Hills").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_hillTestRoll2(){
	var curves = [];

	var SPAN = 8;
	function weight(r){
		/*
		// Quadratic ease-out. Pretty steep!
		var r_i = 1.0-r;
		return 1.0 - (r_i*r_i);
		*/
		return Math.sin( Math.PI*0.5 * r );	// Sine easing. Kinda boring
	};
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, weight(i/SPAN) ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, weight((SPAN-i)/SPAN) ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, weight(-i/SPAN) ) ) };
	for( i=0; i<SPAN; i++ ) { curves.push( new THREE.Vector2( 0, weight(-(SPAN-i)/SPAN) ) ) };

	return new Course("Rolling Hills 2").initWithDxDy_array( curves ).addRandomSprites();
}

function createCourse_vanishPointTest() {
	var curves = [];

	var CURVE = 1.0;
	for( var d=0; d<1; d++ ) {
		var dir = (d===0) ? 1 : -1;
		for( var i=0; i<1; i++ ) { curves.push( new THREE.Vector2( 0, 0 ) ) };
		curves.push( new THREE.Vector2( CURVE*dir, 0 ) );
		for( var i=0; i<1; i++ ) { curves.push( new THREE.Vector2( 0,0 ) ) };
		curves.push( new THREE.Vector2( -CURVE*dir, 0 ) );
	}

	return new Course("Vanish Pt Test").initWithDxDy_array( curves ).addRandomSprites();
}
