// format: [leftPos,rightPos, leftUV_x, rightUV_x] << repeat x2
var default_splits = new Float32Array([
	-1, 0,        0,   0.5,
	 0, 1,        0.5,   1
]);

function CourseSegment( in_dx, in_y, in_roll, in_splits ) {
	var self = this;

	self.dx = in_dx;
	self.y = in_y;
	self.roll = in_roll || 0.0;
	self.splits = in_splits || default_splits;

	self.bezierCtrlX = in_dx;
	self.bezierCtrlY = in_y;
}
