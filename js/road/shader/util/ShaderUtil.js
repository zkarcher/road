function shaderUtil_loadTexture(url) {
	return new Promise( function(resolve, reject){

		var loader = new THREE.TextureLoader();

		loader.load(
			url,

			function (tx) {	// Success
				resolve(tx);
			},

			function (xhr) {	// Loading progress
			},

			function (xhr) {	// Error
				console.log("** shaderUtil_loadTexture: error:", url, xhr);
				reject(xhr);
			}
		)
	});
}

function shaderUtil_loadTextFile(path) {
	return new Promise( function(resolve, reject){

		$.get(path, function(data){
			resolve(data);
		})

		.fail( function(){
			console.log("** shaderUtil_loadTextFile error:", path, this);
			reject();
		});

	});
}

function shaderUtil_loadShaderFile(filename) {
	return new Promise( function(resolve, reject){

		$.get("shader/" + filename, function(data){
			resolve(data);
		})

		.fail( function(){
			console.log("** shaderUtil_loadShaderFile error:", filename, this);
			reject();
		});

	});
}

// vert, frag are filenames (ex: "dither.vert")
function shaderUtil_loadGLSL(vert, frag, materialParams) {
	// Workaround for .all -> .then timing :(
	return new Promise( function(resolve,reject){
		Promise.all(
			[
				shaderUtil_loadShaderFile(vert),
				shaderUtil_loadShaderFile(frag),
			]

		).then(
			function(values) {	// Success
				materialParams['vertexShader'] = values[0];
				materialParams['fragmentShader'] = values[1];

				resolve( new THREE.RawShaderMaterial(materialParams) );
			},

			function(reason) {	// Fail
				console.log("** shaderUtil_loadGLSL fail:", reason);
				reject(reason);
			}
		);
	});
}
