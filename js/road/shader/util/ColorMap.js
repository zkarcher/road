function ColorMap( inPropPrefix, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;

	var canvas = document.getElementById( propPrefix + "canvas" );
	var context = canvas.getContext("2d");
	var colorData = new Uint8Array( 3 * 256 * 256 );	// updated in updatePalette()

	self.colorTexture = new THREE.DataTexture( colorData, 256, 256, THREE.RGBFormat, THREE.UnsignedByteType, THREE.UVMapping, THREE.RepeatWrapping, THREE.RepeatWrapping, THREE.NearestFilter, THREE.NearestFilter, 1 );
	// When surface sample is 0.0/1.0, don't wrap around to other edge
	self.colorTexture.wrapT = THREE.ClampToEdgeWrapping;
	self.colorTexture.needsUpdate = true;

	var buffer = document.createElement('canvas');
	buffer.width = 256;
	buffer.height = 256;
	var bufferContext = buffer.getContext('2d');

	var stops = [];
	var hsvs = [];

	self.updatePalette = function( doReroll ){
		//console.log("DO REROLL", doReroll);
		//var palettes = [ 'test', 'goth', 'red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'purple', 'warm_grey', 'cool_grey', ];

		function rgb2hsv( r, g, b ) {
			return [0.4,0.4,0.4];	// FIXME
		}

		function hsv2rgb( h, s, v ) {
			// Optimization: Handle black quickly
			if( v == 0.0 ) return [0,0,0];

			// Convert h, s, v to RGB.
			// Expand H by 6 times for the 6 color regions. Wrap the remainder to 0..1
			var hSix = wrap( h * 6.0, 6.0 );	// 0..6
			var hDec = hSix - Math.floor( hSix );

			//var v = v;						// top (max) component
			var p = v * (1.0 - s);				// bottom (min) component
			var q = v * (1.0-(hDec*s));			// falling component (yellow->green: the red channel falls)
			var t = v * (1.0 - ((1.0-hDec)*s));	// rising component (red->yellow: the green channel rises)

			// Prepare for HSV output
			var r,g,b;

			if( hSix < 1.0 ) { 		r=v; g=t; b=p; }	// red -> yellow
			else if( hSix < 2.0 ) {	r=q; g=v; b=p; }	// yellow -> green
			else if( hSix < 3.0 ) {	r=p; g=v; b=t; }	// green -> cyan
			else if( hSix < 4.0 ) {	r=p; g=q; b=v; }	// cyan -> blue
			else if( hSix < 5.0 ) {	r=t; g=p; b=v; }	// blue -> magenta
			else {					r=v; g=p; b=q; }	// magenta -> red

			return [r,g,b];
		}

		function setPaletteHSVs( ar ) {
			// Make a rando gradient texture. Use 16 random locations for the color stops.
			if( doReroll ) {
				stops = [0.0];
				for( var i=0; i<15; i++ ) {
					//stops.push( rand() );
					stops.push( i / 16.0 ) + rand()*(1.0/32);
				}
				stops.sort();	// lowest to highest
			}

			function round255( n ){ return Math.round( n * 0xff ) };

			// Assuming 8 colors per palette
			var grd = context.createLinearGradient( 0, 0, 256, 0 );
			for( var c=0; c<8; c++ ) {
				var rgb = hsv2rgb( ar[c][0], ar[c][1], ar[c][2] );
				var color = 'rgb(' + round255(rgb[0]) + ',' + round255(rgb[1]) + ',' + round255(rgb[2]) + ')';
				grd.addColorStop( stops[c*2  ], color );
				grd.addColorStop( stops[c*2+1], color );

				// Return to the first color at the end
				if( c==0 ) grd.addColorStop( 1.0, color );
			}
			context.fillStyle = grd;
			context.fillRect( 0, 0, 256, 256 );

			// In the buffer: Prepare the "far" version of the palette, with offset colors
			var hOffset = props[ propPrefix+'offset_h' ];
			var sOffset = props[ propPrefix+'offset_s' ];
			var vOffset = props[ propPrefix+'offset_v' ];

			var grd = bufferContext.createLinearGradient( 0, 0, 256, 0 );
			for( var c=0; c<8; c++ ) {
				var rgb = hsv2rgb( ar[c][0]+hOffset, clamp( ar[c][1]+sOffset, 0.0, 1.0 ), clamp( ar[c][2]+vOffset, 0.0, 1.0 ) );
				var color = 'rgb(' + round255(rgb[0]) + ',' + round255(rgb[1]) + ',' + round255(rgb[2]) + ')';
				grd.addColorStop( stops[c*2  ], color );
				grd.addColorStop( stops[c*2+1], color );

				// Return to the first color at the end
				if( c==0 ) grd.addColorStop( 1.0, color );
			}
			bufferContext.globalCompositeOperation = "source-over";
			bufferContext.fillStyle = grd;
			bufferContext.fillRect( 0, 0, 256, 256 );

			// In the buffer: Draw gradient from white to transparent, far to near
			var grd = bufferContext.createLinearGradient( 0, 0, 0, 256 );
			var curvePower = props[ propPrefix + 'color_curve' ] || 1.0;
			for( var f=0.0; f<=1.0; f+=0.1 ) {
				grd.addColorStop( f, "rgba(255,255,255," + Math.pow( f, curvePower ) + ")" );
			}
			bufferContext.globalCompositeOperation = "destination-out";
			bufferContext.fillStyle = grd;
			bufferContext.fillRect( 0, 0, 256, 256 );

			// Composite
			context.drawImage( buffer, 0, 0 );

			// FIXME
			// https://stackoverflow.com/questions/2359537/how-to-change-the-opacity-alpha-transparency-of-an-element-in-a-canvas-elemen
			// http://kaioa.com/node/104  <-- this one

			// Convert ArrayBuffer to Uint8Array, WebGL needs correct format
			var imgData = context.getImageData( 0, 0, 256, 256 ).data;
			for ( var i=0; i < 256 * 256; i++ ) {
				var i4 = i*4;
				var i3 = i*3;
				colorData[i3  ] = imgData[i4  ];
				colorData[i3+1] = imgData[i4+1];
				colorData[i3+2] = imgData[i4+2];
			}
			self.colorTexture.needsUpdate = true;
		}

		function createPaletteAroundHSV( h, s, v ) {
			if( doReroll ) {
				var hRand = randRange( 0.0, 0.25 );
				var sRand = randRange( 0.15, 0.25 );
				var vRand = randRange( 0.15, 0.4 );
				console.log("Random palette :: H="+h, "+/-", hRand.toFixed(3), ", S="+s, "+/-", sRand.toFixed(3), ", V="+v, "+/-", vRand.toFixed(3) );

				hsvs = [];
				for( var i=0; i<8; i++ ) {
					hsvs.push([
						h + randBi(hRand),
						clamp( s + randBi(sRand), 0, 1 ),
						clamp( v + randBi(vRand), 0, 1 )
					]);
				}
			}

			setPaletteHSVs( hsvs );
		}

		function createPaletteWhites() {
			if( doReroll ) {
				hsvs = [];
				for( var i=0; i<8; i++ ) {
					hsvs.push( [ rand(), randRange(0.0,0.2), randRange(0.8,1.0) ]);
				}
			}
			setPaletteHSVs( hsvs );
		}

		switch( props[propPrefix+'palette'] ) {
			case 'test':
				setPaletteHSVs( [[0.2,0.1,0.4], [0.0,0.3,0.8], [0.0,0.5,0.0], [0.3,0.7,0.0], [0.6,1.0,0.4], [0.8,0.8,0.8], [0.9,0.4,0.9], [1.0,0.6,0.0]] );
				break;

			case 'goth':
				setPaletteHSVs( [[0,0,0.1], [0,0.1,0.6], [0.9,0.5,0.8], [0.8,0.4,0.3], [0,0.1,0.4], [0.99,0.2,0.2], [0.667,0.2,0.3], [0.9,0.5,0.3]] );
				break;

			case 'red':        createPaletteAroundHSV( 0.0, 0.7, 0.6 ); break;
			case 'orange':     createPaletteAroundHSV( 0.08, 0.5, 0.7 ); break;
			case 'yellow':     createPaletteAroundHSV( 0.2, 0.8, 0.8 ); break;
			case 'green':      createPaletteAroundHSV( 0.4, 0.8, 0.7 ); break;
			case 'cyan':       createPaletteAroundHSV( 0.5, 0.4, 0.7 ); break;
			case 'blue':       createPaletteAroundHSV( 0.667, 0.8, 0.4 ); break;
			case 'purple':     createPaletteAroundHSV( 0.9, 0.75, 0.66 ); break;
			case 'warm_grey':  createPaletteAroundHSV( 0.1, 0.2, 0.5 ); break;
			case 'cool_grey':  createPaletteAroundHSV( 0.6, 0.2, 0.5 ); break;

			case 'whites':     createPaletteWhites(); break;

			default:
				console.log("** Can't handle this palette:", propPrefix, props );
		}
	}
	self.updatePalette( true );

}
