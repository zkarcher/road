var HeadlightDitherShader = function( spriteGeom, inSpriteTexture, inHeadlightTexture, inPropPrefix, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;
	var headlightTexture = inHeadlightTexture;
	var ditherTexture = null;

	self.uniforms = null;
	var colorMap = new ColorMap( propPrefix, props );

	self.updatePalette = function( doReroll ) {
		colorMap.updatePalette( doReroll );
	}

	self.updateHeadlightTexture = function( tx ) {
		//ditherTexture = THREE.ImageUtils.loadTexture('p/course/' + props[propPrefix+'dither_texture'] );
		tx.wrapS = tx.wrapT = THREE.ClampToEdgeWrapping;//THREE.RepeatWrapping;
		tx.format = THREE.LuminanceFormat;	// We only need 1 channel

		tx.magFilter = tx.minFilter = THREE.NearestFilter;
		tx.generateMipmaps = false;

		if( self.material ) self.material.uniforms['headlightTexture'].value = tx;
	}
	self.updateHeadlightTexture( headlightTexture );

	self.updateDitherTexture = function() {
		shaderUtil_loadTexture('p/course/' + props[propPrefix+'dither_texture']).then(
			function(tx) {
				ditherTexture = tx;

				ditherTexture.wrapS = ditherTexture.wrapT = THREE.RepeatWrapping;
				ditherTexture.format = THREE.LuminanceFormat;	// We only need 1 channel

				ditherTexture.magFilter = ditherTexture.minFilter = THREE.NearestFilter;
				ditherTexture.generateMipmaps = false;

				if( self.material ) self.material.uniforms['ditherTexture'].value = ditherTexture;
			}
		)
	}
	self.updateDitherTexture();

	self.uniforms = {
		ditherTexture:     { type: 't', value: ditherTexture },
		headlightTexture:  { type: 't', value: headlightTexture },
		colorTexture:      { type: 't', value: colorMap.colorTexture },
		txScale:           { type: 'f', value: 0.25 },
		headlightScale:    { type: 'f', value: 0.002 },
		distanceY:         { type: 'f', value: 2.5 },
		distanceScale:     { type: 'f', value: -0.00005 },
		ditherTunnel:      { type: 'f', value: 1.0 },
		travelOffset:      { type: 'f', value: 0.0 },
		steering:          { type: 'v2', value: new THREE.Vector2() },
	};

	var spriteTexture = inSpriteTexture;
	spriteTexture.magFilter = spriteTexture.minFilter = THREE.NearestFilter;
	spriteTexture.generateMipmaps = false;
	self.uniforms['spriteTexture'] = { type: 't', value: spriteTexture };

	self.init = function() {
		return shaderUtil_loadGLSL(
			"headlightDither.vert",
			"headlightDither.frag",
			{
				uniforms: self.uniforms,
				depthTest: true,
				depthWrite: true,
				transparent: true,
				side:THREE.DoubleSide
			}
		).then(
			function(material) {
				self.material = material;
			}
		);
	}

}
