var HeadlightShader = function( spriteGeom, inSpriteTexture, inHeadlightTexture, inPropPrefix, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;
	var headlightTexture = inHeadlightTexture;

	self.uniforms = null;
	var colorMap = new ColorMap( propPrefix, props );

	var vertexShader_ar = [
		"precision mediump float;",
		"precision mediump int;",
		"uniform mat4 modelViewMatrix;",
		"uniform mat4 projectionMatrix;",
		"attribute vec3 position;",
		"attribute vec2 uv;",
		"uniform float txScale;",
		"uniform float distanceY;",
		"uniform float distanceScale;",
		"uniform float travelOffset;",
		"uniform vec2 steering;",
		"uniform sampler2D colorTexture;",

		"varying vec2 v_spriteUV;",
		"varying vec2 v_headlightUV_far;",
		"varying vec2 v_headlightUV_near;",
		"varying vec4 v_color;",
		"varying vec4 v_hilite;",

		"void main()	{",
		"	gl_Position = projectionMatrix * modelViewMatrix * vec4( position, 1.0 );",
		" v_spriteUV = uv;",
		" float useTxScale = txScale + distanceScale * position.z;",

		// Precompute gl_FragCoord.z equivalent in vertexShader, for faster colorTexture sampling
		// Vertex in clip-space:
		" vec4 fake_frag_coord = (modelViewMatrix * vec4(position,1.0));", // Range:   [-w,w]^4

		// Vertex in NDC-space
		" fake_frag_coord.xyz /= fake_frag_coord.w;", // Rescale: [-1,1]^3
		" fake_frag_coord.w    = 1.0 / fake_frag_coord.w;", // Invert W

		// Vertex in window-space
		" fake_frag_coord.xyz *= vec3 (0.5) + vec3 (0.5);", // Rescale: [0,1]^3
		//" fake_frag_coord.xy  *= window;",                  // Scale and Bias for Viewport

		" vec2 colorUV = vec2( travelOffset, fake_frag_coord.z );",
		" v_color = texture2D( colorTexture, colorUV );",
		" v_hilite = vec4( 1.0, 1.0, 1.0, 1.0 );",

		" v_headlightUV_far = (position.xy - vec2(0.0,position.z*distanceY)) * useTxScale + vec2(0.5,0.48) + steering*position.z;",
		" v_headlightUV_near = (position.xy - vec2(0.0,position.z*distanceY)) * useTxScale * 0.85 + vec2(0.5,0.48) + steering*position.z*0.85;",

		"}",
	];
	var vertexShaderGLSL = vertexShader_ar.join("\n");

	var fragmentShader_ar = [
		"precision mediump float;",
		"precision mediump int;",
		"uniform sampler2D spriteTexture;",
		"uniform sampler2D headlightTexture;",
		//"uniform sampler2D colorTexture;",

		"varying vec2 v_spriteUV;",
		"varying vec2 v_headlightUV_far;",
		"varying vec2 v_headlightUV_near;",
		"varying vec4 v_color;",
		"varying vec4 v_hilite;",

		"void main()	{",
		" vec2 sprite = vec2( texture2D( spriteTexture, v_spriteUV ).ra );",
		" if( sprite.y < 0.05 ) discard;",

		//" vec4 color = texture2D( colorTexture, v_colorUV );",

		" vec4 headlight = texture2D( headlightTexture, mix( v_headlightUV_far, v_headlightUV_near, sprite.x ) );",

		" gl_FragColor = vec4( (v_color.rgb * (headlight.r*2.0)) + (v_hilite.rgb * clamp( headlight.r*2.0-1.0, 0.0, 1.0 ) ), sprite.y );",
		/*
		" if( headlight.r < 0.5 ) {",
		"  gl_FragColor = ( v_color * (headlight.r*2.0) ) * sprite.zzzy;",
		" } else {",
		"  gl_FragColor = ( v_color + v_hilite * (headlight.r*2.0-1.0) ) * sprite.zzzy;",
		" }",
		*/
		//" gl_FragColor = vec4( sprite.xxxy );",	// z-fighting test
		"}",
	];

	var fragmentShaderGLSL = fragmentShader_ar.join("\n");

	self.updatePalette = function( doReroll ) {
		colorMap.updatePalette( doReroll );
	}

	self.updateHeadlightTexture = function( tx ) {
		//ditherTexture = THREE.ImageUtils.loadTexture('p/course/' + props[propPrefix+'dither_texture'] );
		tx.wrapS = tx.wrapT = THREE.RepeatWrapping;
		tx.format = THREE.LuminanceFormat;	// We only need 1 channel

		tx.magFilter = tx.minFilter = THREE.NearestFilter;
		tx.generateMipmaps = false;

		if( self.material ) self.material.uniforms['headlightTexture'].value = tx;
	}
	self.updateHeadlightTexture( headlightTexture );

	self.uniforms = {
		headlightTexture:  { type: 't', value: headlightTexture },
		colorTexture:      { type: 't', value: colorMap.colorTexture },
		txScale:           { type: 'f', value: 0.002 },
		distanceY:         { type: 'f', value: 2.5 },
		distanceScale:     { type: 'f', value: -0.00005 },
		travelOffset:      { type: 'f', value: 0.0 },
		steering:          { type: 'v2', value: new THREE.Vector2() },
	};

	var spriteTexture = inSpriteTexture;
	spriteTexture.magFilter = spriteTexture.minFilter = THREE.NearestFilter;
	spriteTexture.generateMipmaps = false;
	self.uniforms['spriteTexture'] = { type: 't', value: spriteTexture };

	self.material = new THREE.RawShaderMaterial({
		vertexShader: vertexShaderGLSL,
		fragmentShader: fragmentShaderGLSL,
		uniforms: self.uniforms,
		depthTest: true,
		depthWrite: true,
		transparent: true,
		side:THREE.DoubleSide
	});

}
