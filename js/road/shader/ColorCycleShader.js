var ColorCycleShader = function( inTexture, inPropPrefix, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;

	self.uniforms = null;
	var colorMap = new ColorMap( propPrefix, props );

	self.updatePalette = function( doReroll ) {
		colorMap.updatePalette( doReroll );
	}

	self.uniforms = {
		colorLoops:   { type: 'f', value: 1.0 },
		colorTexture: { type: 't', value: colorMap.colorTexture },
		travelOffset: { type: 'f', value: 0.0 },
	};

	var surfaceTexture = inTexture;
	surfaceTexture.magFilter = surfaceTexture.minFilter = THREE.NearestFilter;
	surfaceTexture.generateMipmaps = false;
	self.uniforms['surfaceTexture'] = { type: 't', value: surfaceTexture };

	self.init = function() {
		return shaderUtil_loadGLSL(
			"colorCycle.vert",
			"colorCycle.frag",
			{
				uniforms: self.uniforms,
				depthTest: true,
				depthWrite: true,
				transparent: true,
				side:THREE.DoubleSide
			}
		).then(
			function(material) {
				self.material = material;
			}
		);
	}

}
