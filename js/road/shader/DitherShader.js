var DitherShader = function( inPropPrefix, inColorMapMode, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;

	var isRoad = (inColorMapMode === "road");	// "surface brightness to Y"
	var isGround = (inColorMapMode === "ground");	// "distance to Y"

	var ditherTexture = null;	// THREE.Texture
	self.uniforms = null;
	var colorMap = new ColorMap( propPrefix, props );

	//
	//  TEXTURES
	//
	self.updateDitherTexture = function() {
		return shaderUtil_loadTexture('p/course/' + props[propPrefix+'dither_texture'])
		.then(
			function(tx){
				tx.wrapS = tx.wrapT = THREE.RepeatWrapping;
				tx.format = THREE.LuminanceFormat;	// We only need 1 channel

				tx.magFilter = tx.minFilter = THREE.NearestFilter;
				tx.generateMipmaps = false;

				if( self.material ) self.material.uniforms['ditherTexture'].value = tx;
			},

			function(reason) {
				console.log("** updateDitherTexture reject:", reason);
			}
		);
	}

	self.updatePalette = function( doReroll ) {
		colorMap.updatePalette( doReroll );
	}

	self.uniforms = {
		colorLoops:   { type: 'f', value: 1.0 },
		ditherTexture: { type: 't', value: ditherTexture },
		colorTexture: { type: 't', value: colorMap.colorTexture },
		overlap:      { type: 'f', value: 1.0 },
		travelOffset: { type: 'f', value: 0.0 },
		depthCurve:   { type: 'f', value: 8.0 },
		txScale:      { type: 'f', value: 0.1 },
		octaveSize0:  { type: 'f', value: 0.0 },
		octaveAlpha0: { type: 'f', value: 0.0 },
		octaveSize1:  { type: 'f', value: 0.0 },
		octaveAlpha1: { type: 'f', value: 0.0 },
		octaveCenter: { type: 'v2', value: new THREE.Vector2() },
		octaveScroll: { type: 'v3', value: new THREE.Vector3() },
		skewMult:     { type: 'f', value: 1.0 },
		farCrunch:    { type: 'v2', value: new THREE.Vector2() },
		isRoad:       { type: 'b', value: isRoad },
		surfaceTexture:{ type: 't', value: null },
	};

	self.init = function() {
		var promises = [
			self.updateDitherTexture(),

			shaderUtil_loadGLSL(
				"dither.vert",
				"dither.frag",
				{
					uniforms: self.uniforms,
					depthTest: true,
					depthWrite: true,
					side:THREE.DoubleSide
				}
			).then(
				function(material) {
					self.material = material;
				}
			),
		];

		// Road texture?
		if (isRoad) {
			promises.push(
				shaderUtil_loadTexture('p/course/desert_awesome_pavement_bw_noise0.03.png')
				.then(
					function(tx) {
						tx.wrapS = tx.wrapT = THREE.RepeatWrapping;
						tx.format = THREE.LuminanceFormat;	// We only need 1 channel
						tx.magFilter = tx.minFilter = THREE.NearestFilter;
						tx.generateMipmaps = false;
						self.uniforms['surfaceTexture'].value = tx;
					}
				)
			);
		}

		return Promise.all(promises);
	}

	//
	//  EVENTS
	//
	self.onWindowResize = function(){
		var w = window.innerWidth;
		var h = window.innerHeight;
		self.uniforms['octaveCenter'].value.set( w/2, h/2 );
	}

}
