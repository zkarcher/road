var BalefireShader = function( spriteGeom, inFireTexture, inPropPrefix, inProps ){
	var self = this;
	var propPrefix = inPropPrefix;
	var props = inProps;

	var ditherTexture = null;	// THREE.Texture
	self.uniforms = null;
	var colorMap = new ColorMap( propPrefix, props );

	self.updateDitherTexture = function() {
		shaderUtil_loadTexture('p/course/' + props[propPrefix+'dither_texture']).then(
			function(tx) {
				ditherTexture = tx;

				ditherTexture.wrapS = ditherTexture.wrapT = THREE.RepeatWrapping;
				ditherTexture.format = THREE.LuminanceFormat;	// We only need 1 channel

				ditherTexture.magFilter = ditherTexture.minFilter = THREE.NearestFilter;
				ditherTexture.generateMipmaps = false;

				if( self.material ) self.material.uniforms['ditherTexture'].value = ditherTexture;
			}
		);
	}
	self.updateDitherTexture();

	self.updatePalette = function( doReroll ) {
		colorMap.updatePalette( doReroll );
	}

	self.uniforms = {
		ditherTexture:  { type: 't', value: ditherTexture },
		colorTexture: { type: 't', value: colorMap.colorTexture },
		travelOffset: { type: 'f', value: 0.0 },
		colorLoops:   { type: 'f', value: 1.0 },
		txScale:        { type: 'f', value: 0.1 },
		txScroll:       { type: 'v2', value: new THREE.Vector2() },
		txHardEdge:     { type: 'f', value: 1.0 },
		ripplePhase:    { type: 'f', value: 0.0 },
		rippleFreq:     { type: 'f', value: 4.0 },
		rippleStrength: { type: 'f', value: 0.5 },
	};

	var fireTexture = inFireTexture;
	fireTexture.magFilter = fireTexture.minFilter = THREE.NearestFilter;
	fireTexture.generateMipmaps = false;
	fireTexture.format = THREE.LuminanceFormat;
	self.uniforms['fireTexture'] = { type: 't', value: fireTexture };

	self.init = function() {
		return shaderUtil_loadGLSL(
			"balefire.vert",
			"balefire.frag",
			{
				uniforms: self.uniforms,
				depthTest: true,
				depthWrite: true,
				transparent: true,
				side:THREE.DoubleSide
			}
		).then(
			function(material) {
				self.material = material;
			}
		);
	}

}
