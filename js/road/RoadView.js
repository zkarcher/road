// RoadView
//
//   Contains Sprites and other viewable objects which will be rendered
//   by RoadRenderer.

function RoadView() {
	var self = this;

	self.sprites = [];
	self.bgSprites = [];

	self.reset = function() {
		self.sprites = [];
		self.bgSprites = [];
	}

	self.perFrame = function(timeMult, course, roadProgress) {
		/*
		var roadDelta = roadProgress - lastRoadProgress;

		for( var i=0, len=self.spriteGroups.length; i<len; i++ ) {
			self.spriteGroups[i].updateDZ( roadDelta );
		}
		for( var i=0, len=self.sprites.length; i<len; i++ ) {
			self.sprites[i].updateDZ( roadDelta );
		}
		*/

		// Update the array of visible Sprites
		var courseSprites = course.getSprites();
		for(var i = 0; i < courseSprites.length; i++) {
			var spr = courseSprites[i];
			var z = spr.loc.z;

			var isInViewingRange = (0 >= z) && (z >= MAX_DRAW_DISTANCE_SPRITES);
			if (isInViewingRange && !spr.isInView) {
				self.sprites.push(spr);

			} else if (!isInViewingRange && spr.isInView) {
				var idx = self.sprites.indexOf(spr);
				if (idx >= 0) {
					self.sprites.splice(idx, 1);
				}
			}

			spr.isInView = isInViewingRange;
		}
	}
}
