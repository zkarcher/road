function RoadSettings(road) {
	var self = this;

	// GUI, property keys
	var gui, props={};

	self.getProps = function(){ return props; }

	self.get = function(key){ return props[key]; }

	self.initGUI = function() {
		gui = new dat.GUI();
		var folder;

		function addPropSlider( key, value, min, max, step, onChange_func ){
			props[ key ] = value;
			var ctrlr = folder.add( props, key, min, max ).step( step );
			if( onChange_func ) ctrlr.onChange( onChange_func );
		}

		function addPropDropdown( key, value, options, onChange_func ) {
			props[ key ] = value;
			var ctrlr = folder.add( props, key, options );
			if( onChange_func ) ctrlr.onChange( onChange_func );
		}

		function addPropButton( key, func ) {
			props[ key ] = func;
			folder.add( props, key );
		}

		function addCheckbox( key, value ) {
			props[ key ] = Boolean(value);
			return folder.add( props, key ).listen();
		}

		folder = gui.addFolder( "Pseudo 3D <3s U" );
		addCheckbox( BATTERY_SAVER, true );
		addPropSlider( DRIVE_SPEED, DEFAULT_DRIVE_SPEED,   0, MAX_DRIVE_SPEED,   1,   road.setSliderdrive_speed );
		addPropDropdown( DRAW_PRECISION, PRECISION_2000S, [PRECISION_1980S,PRECISION_1990S,PRECISION_2000S] );
		folder.open();

		folder = gui.addFolder( "Keyboard Controls" );
		addPropSlider( KEYBOARD_TIGHT_STEERING,  0.88,    0.1, 1.0,    0.01 );
		addPropSlider( KEYBOARD_MAX_STEER_DX, 8.0, 1.0, 10.0,    0.1 );
		addPropSlider( KEYBOARD_STEERING_DECEL, 0.7,      0.2, 1.0,   0.01 );
		addPropSlider( KEYBOARD_HARSH_CURVES, 2.7,   0.0, 20.0,   0.1 );
		addPropSlider( KEYBOARD_HARSH_ROLL, 7.0, 0.0, 20.0, 0.1 );

		function addShaderProps( title, prefix, clss, defaults ){
			folder = gui.addFolder( title );

			function getDef( prop, val ){ return defaults.hasOwnProperty(prop) ? defaults[prop] : val; }

			addPropSlider( prefix+COLOR_LOOPS, getDef(COLOR_LOOPS,2.0), 0.0, 8.0, 0.25 );

			if( clss===DitherShader ) {
				addPropSlider( prefix+OVERLAP, getDef(OVERLAP,0.6), 0.0, 2.0, 0.05 );
			}

			addPropSlider( prefix+TRAVEL, getDef(TRAVEL,0.05), -0.15, 0.15, 0.01 );

			if( clss===DitherShader ) {
				addPropSlider( prefix+DEPTH_CURVE, getDef(DEPTH_CURVE,1.75), 0.0, 20.0, 0.25 );
			}

			if( clss===DitherShader || clss===BalefireShader || clss===HeadlightDitherShader ) {
				var txs = [ 'RockSharp0022_1_S.jpg', 'BarkDecidious0194_7_S.jpg', 'BarkStripped0021_2_S.jpg', 'BarkStripped0021_2_S_rot.jpg', 'Bones0029_2_S.jpg', 'dither_4x4.png', 'dither_4x4_1b2.png', 'dither_4x4_rocks.png', 'gears_1.jpg', 'gears_2.jpg', 'GrassTall0002_S.jpg', 'GravelSpecial0022_1_S.jpg', 'Hedges0035_2_S.jpg', 'MarbleGreen0001_39_S.jpg', 'MarbleTiles0066_1_S.jpg', 'MarbleWhite0066_13_S.jpg', 'NatureDesert0001_S.jpg', 'Skull_tile-2.jpg', 'Snow0065_6_S.jpg', 'Snow0070_7_S.jpg', 'Waterplants0040_5_S.jpg', 'WoodFine0007_S.jpg', 'WoodRough0021_2_S.jpg', 'WoodRough0050_3_S.jpg', 'WoodRough0106_2_S.jpg' ];
				addPropDropdown( prefix+DITHER_TEXTURE, getDef(DITHER_TEXTURE,txs[0]), txs, function(){road.updateDitherTexture(prefix)} );
			}

			var palettes = [ 'test', 'goth', 'red', 'orange', 'yellow', 'green', 'cyan', 'blue', 'purple', 'warm_grey', 'cool_grey', 'whites' ];
			addPropDropdown( prefix+PALETTE, getDef(PALETTE,'purple'), palettes, function(){road.updatePalette(prefix,true)} );
			addPropButton( prefix+REROLL_PALETTE, function(){road.updatePalette(prefix,true)} );

			if( clss===DitherShader || clss===BalefireShader || clss===HeadlightShader || clss===HeadlightDitherShader ) {
				addPropSlider( prefix+TX_SCALE, getDef(TX_SCALE,0.64), 0.0, 1.0, 0.01 );
			}

			if( clss===DitherShader ) {
				addPropSlider( prefix+OCTAVE_SPEED, getDef(OCTAVE_SPEED,0.45), -1.0, 1.0, 0.05 );
				addPropSlider( prefix+OCTAVE_SIZE, getDef(OCTAVE_SIZE,2.9), 1.0, 4.0, 0.05 );
				addPropSlider( prefix+OCTAVE_SCROLL_X, getDef(OCTAVE_SCROLL_X,0.25), -1.0, 1.0, 0.05 );
				addPropSlider( prefix+OCTAVE_SCROLL_Y, getDef(OCTAVE_SCROLL_Y,10.0), -20.0, 20.0, 0.5 );
				addPropSlider( prefix+OCTAVE_ROLL, getDef(OCTAVE_ROLL,1.0), -2.0, 2.0, 0.1 );
				addPropSlider( prefix+STRAFE_AMT, getDef(STRAFE_AMT,1.0), -2.0, 4.0, 0.25 );
				addPropSlider( prefix+SKEW_MULT, getDef(SKEW_MULT,1.0), -2.0, 2.0, 0.25 );
				addPropSlider( prefix+FAR_CRUNCH_X, getDef(FAR_CRUNCH_X,0.0), 0.0, 24.0, 0.5 );
				addPropSlider( prefix+FAR_CRUNCH_Y, getDef(FAR_CRUNCH_Y,0.0), 0.0, 24.0, 0.5 );
			}

			addPropSlider( prefix+OFFSET_H, getDef(OFFSET_H,0), -0.5, 0.5, 0.05, function(){road.updatePalette(prefix,false)} );
			addPropSlider( prefix+OFFSET_S, getDef(OFFSET_S,0.35), -1.0, 1.0, 0.05, function(){road.updatePalette(prefix,false)} );
			addPropSlider( prefix+OFFSET_V, getDef(OFFSET_V,-0.55), -1.0, 1.0, 0.05, function(){road.updatePalette(prefix,false)} );

			if( clss===DitherShader || clss===SpriteShader || clss===BalefireShader || clss===HeadlightShader || clss===HeadlightDitherShader ) {
				addPropSlider( prefix+COLOR_CURVE, getDef(COLOR_CURVE,3.0), 0.1, 5.0, 0.1, function(){road.updatePalette(prefix,false)});
			}

			if( clss===BalefireShader ) {
				addPropSlider( prefix+TX_SCROLL_X, getDef(TX_SCROLL_X,0.0), -2.0, 2.0, 0.2 );
				addPropSlider( prefix+TX_SCROLL_Y, getDef(TX_SCROLL_Y,0.6), -2.0, 2.0, 0.2 );
				addPropSlider( prefix+TX_HARD_EDGE, getDef(TX_HARD_EDGE,2.0), 1.0, 10.0, 0.5 );

				addPropSlider( prefix+RIPPLE_SPEED, getDef(RIPPLE_SPEED,0.14), 0.0, 0.5, 0.01 );
				addPropSlider( prefix+RIPPLE_FREQ, getDef(RIPPLE_FREQ,17.0), 0.0, 50.0, 1.0 );
				addPropSlider( prefix+RIPPLE_STRENGTH, getDef(RIPPLE_STRENGTH,0.11), 0.0, 0.5, 0.01 );
			}

			if( clss===HeadlightShader || clss===HeadlightDitherShader ) {
				addPropSlider( prefix+DISTANCE_Y, getDef(DISTANCE_Y,0.0), -10.0, 10.0, 0.25 );
				addPropSlider( prefix+DISTANCE_SCALE, getDef(DISTANCE_SCALE,0.0), -2.0, 2.0, 0.1 );

				var txs = ['headlight_map3.png','sigil_2.png','trollface.png'];
				addPropDropdown( prefix+HEADLIGHT_TEXTURE, getDef(HEADLIGHT_TEXTURE,txs[0]), txs, function(){road.updateHeadlightTexture(prefix)} );

				addPropSlider( prefix+HEADLIGHT_SCALE, getDef(HEADLIGHT_SCALE,0.64), 0.0, 1.0, 0.01 );

				addPropSlider( prefix+DITHER_TUNNEL, getDef(DITHER_TUNNEL,1.0), 0.0, 3.0, 0.1 );
			}
			//folder.open();
		}

		addShaderProps( 'Ground Shader', 'G:', DitherShader, {
			'palette': 'cyan',
			'dither_texture': 'GrassTall0002_S.jpg',
		});
		addShaderProps( 'Road Shader', 'R:', DitherShader, {
			'palette': 'purple',
			'color_loops': 1,
			//'travel': 0,
			'overlap': 0.25,
			'tx_scale': 0,
			'dither_texture': 'dither_4x4.png',
			'octave_speed': 0.65,
			'octave_size': 1,
			'octave_scroll_x': 0.5,
			'octave_scroll_y': 10,
			'octave_roll': 0.5,
			'strafe_amt': 1.0,
			'color_curve': 0.6,
			'offset_h':0,
			'offset_s':-0.5,
			'offset_v':-0.8,
		});
		addShaderProps( 'Moon Shader', 'M:', ColorCycleShader, {
			'palette': 'cyan',
			'color_loops': 0.75,
			'travel': -0.04,
			'offset_h': 0,
			'offset_s': 0.1,
			'offset_v': 0.3,
		});
		addShaderProps( 'Sprite Shader', 'S:', SpriteShader, {
			'palette': 'orange',
			'color_loops': 1.0,
			'travel': 0.01,
			'offset_h': 0,
			'offset_s': 0,
			'offset_v': -1.0,
			'color_curve': 0.3,
		});
		addShaderProps( 'Fire Shader', 'F:', BalefireShader, {
			'dither_texture': 'Bones0029_2_S.jpg',
			'tx_scale': 0.74,
			'tx_scroll_x': 0.0,
			'tx_scroll_y': 1.8,
			'tx_hard_edge': 2.5,
			'palette': 'orange',
			'color_loops': 0.25,
			'color_curve': 0.1,
			'offset_h': 0,
			'offset_s': -0.7,
			'offset_v': 0.5,
		});
		addShaderProps( 'Headlight Shader', 'H:', HeadlightDitherShader, {
			'palette': 'red',
			'dither_texture': 'BarkStripped0021_2_S_rot.jpg',
			'tx_scale': 0.83,
			'travel': 0.01,
			'color_curve': 1.6,
			'offset_h': 0,
			'offset_s': 0.55,
			'offset_v': -0.8,
			'headlight_scale': 0.74,
			'distance_y': 2.0,
			'distance_scale': -0.5,
			'dither_tunnel': 1.3,
		});

		folder = gui.addFolder( "Road Geometry" );
		addPropSlider( HORIZON_DISTANCE, 70,   10, 500,   1, road.setHorizonDistance );
		addPropSlider( ROAD_WIDTH, 400,   50, 2000,   50 );
		addPropSlider( TX_SCALE_Y, 0.25,   0, 1.0,   0.05 );
		addPropSlider( DX_REDUCES_Z, 0,   0.0, 0.2,   0.01 );
		//addPropSlider( DX_2_TX_ANGLE, 0.2,   -1.0, 1.0,   0.05 );
		//addPropSlider( INV_COS_2_WIDTH, 0.0,   0.0, 6.0,   0.1 );
		addPropSlider( PITCH_SKEW, 1.0, 0.2, 3.0, 0.1 );
		const CURVE_TYPES = ['bezier','linear','spline','windowed'];
		addPropDropdown( DX_CURVE_TYPE, 'linear', CURVE_TYPES, road.setCurveChange );
		addPropDropdown( Y_CURVE_TYPE, 'spline', CURVE_TYPES, road.setCurveChange );
		addPropDropdown( ROLL_CURVE_TYPE, 'linear', CURVE_TYPES, road.setCurveChange );
		addPropSlider( CONVEYOR_BELT, 1.0,  -4.0, 4.0,   0.1 );

		folder = gui.addFolder( "Background" );
		addPropSlider( BG_SCROLL_X, 0.08,   0.0, 0.3,   0.01 );
		addPropSlider( BG_SCROLL_Y, 0.5,   -3.0, 3.0,   0.1 );
		addPropSlider( BG_ROLL, 1.2,   -1.0, 3.0,   0.1 );

		// ROADS
		folder = gui.addFolder( "Courses" );
		addPropSlider( TURN_STEEP, 20.0, 0.0, 50.0, 0.1 );
		addPropSlider( HILL_STEEP, DEFAULT_HILL_STEEP,   0, 40,   0.1 );
		addPropSlider( ROLL_STEEP, 0.7, 0.0, 4.0, 0.1 );
		addPropSlider( X_OFFSET, 0.0, -(MAX_DRIVE_STRAFE*0.75), MAX_DRIVE_STRAFE*0.75, 5.0,   road.setSliderXOffset );
		addPropSlider( DX, 0.0, -0.5, 0.5, 0.01 );
		addPropSlider( DY, 0.0,  -2.0, 2.0,   0.1 );
		addPropSlider( FIRST_ROLL, 0.0, -1.0, 1.0, 0.01 );
		addPropSlider( SCALE_COURSE, 1.0,  0.25, 4.0,   0.01 );

		var courseFuncs = [createCourse_randomFun, createCourse_testFlat, createCourse_tightTurns, createCourse_hillTest, createCourse_hillTestValleys, createCourse_hillTestLongValleys, createCourse_hillTestRollingHills, createCourse_hillTestRoll2, createCourse_vanishPointTest];
		function createCourseClosure(_func){ return function(){road.setCourse(_func)} };
		for( var i=0; i<courseFuncs.length; i++ ) {
			var _func = courseFuncs[i];
			var result = _func();
			addPropButton( result['title'], createCourseClosure(_func) );
		}
		folder.open();
	}
}
