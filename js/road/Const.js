const MAX_RENDER_WIDTH = 1600;
const MAX_RENDER_HEIGHT = 1600;
const MAX_SPRITES = 2048;

const PRECISION_1980S = '1980s';
const PRECISION_1990S = '1990s';
const PRECISION_2000S = '2000s';

const DEFAULT_DRIVE_SPEED = 80;
const MAX_DRIVE_SPEED = 500;
const MAX_DRIVE_STRAFE = 500;

const DEFAULT_HILL_STEEP = 16.0;

const MAX_ROAD_SPLITS = 2;

const MAX_DRAW_DISTANCE_SPRITES = -100;

const MAX_ROAD_SPLIT_AMT = 2.5;

//
//  SETTINGS
//

const BATTERY_SAVER = 'battery_saver';
const DRIVE_SPEED = 'drive_speed';
const DRAW_PRECISION = 'draw_precision';

const BG_SCROLL_X = 'bg_scroll_x';
const BG_SCROLL_Y = 'bg_scroll_y';
const BG_ROLL = 'bg_roll';
const HORIZON_DISTANCE = 'horizon_distance';
const ROAD_WIDTH = 'road_width';
const TX_SCALE_Y = 'tx_scale_y';
const DX_REDUCES_Z = 'dx_reduces_z';
//const DX_2_TX_ANGLE = 'dx_2_tx_angle';
//const INV_COS_2_WIDTH = 'inv_cos_2_width';
const PITCH_SKEW = 'pitch_skew';
const DX_CURVE_TYPE = 'dx_curve_type';
const Y_CURVE_TYPE = 'y_curve_type';
const ROLL_CURVE_TYPE = 'roll_curve_type';
const CONVEYOR_BELT = 'conveyor_belt';

const TURN_STEEP = 'turn_steep';
const HILL_STEEP = 'hill_steep';
const ROLL_STEEP = 'roll_steep';
const X_OFFSET = 'xOffset (strafe)';
const DX = 'rotate (dx)';
const DY = 'incline (dy)';
const FIRST_ROLL = 'first_roll';
const SCALE_COURSE = 'scale_course';

const KEYBOARD_TIGHT_STEERING = 'tight_steering';
const KEYBOARD_MAX_STEER_DX = 'max_steer_dx';
const KEYBOARD_STEERING_DECEL = 'steering_decel';
const KEYBOARD_HARSH_CURVES = 'harsh_curves';
const KEYBOARD_HARSH_ROLL = 'harsh_roll';

const PALETTE = 'palette';
const REROLL_PALETTE = 'reroll_palette!';
const COLOR_LOOPS = 'color_loops';
const OVERLAP = 'overlap';
const TRAVEL = 'travel';
const DEPTH_CURVE = 'depth_curve';
const DITHER_TEXTURE = 'dither_texture';
const TX_SCALE = 'tx_scale';
const TX_SCROLL_X = 'tx_scroll_x';
const TX_SCROLL_Y = 'tx_scroll_y';
const TX_HARD_EDGE = 'tx_hard_edge';

const RIPPLE_SPEED = 'ripple_speed';
const RIPPLE_FREQ = 'ripple_freq';
const RIPPLE_STRENGTH = 'ripple_strength';

const OCTAVE_SPEED = 'octave_speed';
const OCTAVE_SIZE = 'octave_size';
const OCTAVE_SCROLL_X = 'octave_scroll_x';
const OCTAVE_SCROLL_Y = 'octave_scroll_y';
const OCTAVE_ROLL = 'octave_roll';
const STRAFE_AMT = 'strafe_amt';
const SKEW_MULT = 'skew_mult';
const FAR_CRUNCH_X = 'far_crunch_x';
const FAR_CRUNCH_Y = 'far_crunch_y';

const OFFSET_H = 'offset_h';
const OFFSET_S = 'offset_s';
const OFFSET_V = 'offset_v';
const COLOR_CURVE = 'color_curve';

const DISTANCE_Y = 'distance_y';
const DISTANCE_SCALE = 'distance_scale';
const HEADLIGHT_TEXTURE = 'headlight_texture';
const HEADLIGHT_SCALE = 'headlight_scale';
const DITHER_TUNNEL = 'dither_tunnel';
