const TRUNK_SCALE_MIN = 2.0;
const TRUNK_SCALE_MAX = 2.5;
const BRANCH_COUNT_MIN = 4;
const BRANCH_COUNT_MAX = 7;
const BRANCH_SCALE_MULT = 2.5;
const BRANCH_LOW_Y = 900;
const BRANCH_RAND_X = 0.0;
const BRANCH_STEP_Y_MIN = 700;
const BRANCH_STEP_Y_MAX = 1000;

function SpriteGroup( inScale, inLoc, inKind, inAtlas ){

	var self = this;
	self.loc = inLoc;
	self.scale = inScale;
	self.kind = inKind;
	self.atlas = inAtlas;

	self.sprites = [];
	switch( self.kind ) {
		case 'tree':
			var trunkScale = randRange( TRUNK_SCALE_MIN, TRUNK_SCALE_MAX );

			self.sprites.push( new Sprite( 'headlight', trunkScale * self.scale, self.loc.clone(), self.atlas['tree_trunk'], self ) );

			var branchY = self.loc.y + BRANCH_LOW_Y;
			var branchCount = randRangeInt( BRANCH_COUNT_MIN, BRANCH_COUNT_MAX+1 );
			var branchScale = self.scale * trunkScale * BRANCH_SCALE_MULT;
			var smallProduct = 1.0;
			for( var i=0; i<branchCount; i++ ) {
				var spr = new Sprite(
					'headlight',
					randRange(0.6,0.8) * branchScale, 	// scale
					new THREE.Vector3( self.loc.x, branchY, self.loc.z - i*0.11 - 0.1 ),
					self.atlas[ 'tree_branches' ],
					self	// inform Sprite that it has an owner
				);
				self.sprites.push( spr );

				var getSmaller = randRange( 0.90, 0.95 );
				branchScale *= getSmaller;	// higher branches are smaller
				smallProduct *= getSmaller;
				branchY += randRange( BRANCH_STEP_Y_MIN, BRANCH_STEP_Y_MAX ) * smallProduct;
			}
			break;
	}

	self.updateDZ = function( dz ) {
		self.loc.z += dz;

		// After passing the camera, reset Z somewhere in the distance
		if( self.loc.z > 0 ) {

			var advance = randRange( -100, -80 );
			self.loc.z += advance;

			for( var i=0, len=self.sprites.length; i<len; i++ ) {
				self.sprites[i].addZ( advance );
			}
		}
	}

	self.update = function( drawIdx, spritePositions, spriteUVs, cameraFar, roadPositionAr, firstRollAngle, precision ) {
		if( self.loc.z < cameraFar ) return false;

		/*
		var scale = self.scale / (-self.loc.z);

		if( precision===PRECISION_1980S ) {
			scale = Math.max( 0.01, Math.floor( scale*50 ) * 0.02 );
			scale = Math.min( 0.33, scale );
		} else if( precision===PRECISION_1990S ) {
			scale = Math.max( 0.01, Math.floor( scale*200 ) * 0.005 );
		}

		var locFunc = function(n){ return n; }
		if( precision===PRECISION_1980S ) {
			locFunc = function(n){ return Math.round(n*0.25) * 4.0 };
		} else if( precision===PRECISION_1990S ) {
			locFunc = function(n){ return Math.round(n) };
		}

		// Find the roadPosition that matches our Z. We'll plant the Sprite there.
		var placement = new THREE.Vector3();

		var prevPos = roadPositionAr[0];
		for( var i=0, len=roadPositionAr.length; i<len; i++ ) {
			var nextPos = roadPositionAr[i];
			if( nextPos.z < self.loc.z ) {

				// Interpolate between lastPos and pos, get nice position
				function getScreenPosFromRoadPos( pos ) {
					var screenRatio = ((self.loc.x - pos.centerX) / (-self.loc.z)) * pos.totalRollCos + 0.5;	// 0..1, 0.5==center
					return new THREE.Vector3(
						locFunc( 1000 * (screenRatio-0.5) ),
						locFunc( lerp( pos.drawY_L, pos.drawY_R, screenRatio ) + (h*0.4)*scale ),
						self.loc.z
					);
				}

				var lerpRatio = (self.loc.z - nextPos.z) / (prevPos.z - nextPos.z);
				placement.lerpVectors( getScreenPosFromRoadPos(nextPos), getScreenPosFromRoadPos(prevPos), lerpRatio );

				//console.log( pos.drawY_L, pos.drawY_R, screenRatio, lerp( pos.drawY_L, pos.drawY_R, screenRatio ) );
				break;
			}

			prevPos = nextPos;
		}

		var rotFunc = function(n){ return n; }
		if( precision===PRECISION_1980S ) {
			rotFunc = function(n){ return Math.round(n*4.0) / 4.0 };
		} else if( precision===PRECISION_1990S ) {
			rotFunc = function(n){ return Math.round(n*16.0) / 16.0 };
		}

		// Update positions
		scale *= 0.5;
		var cornerNE = new THREE.Vector2(
			self.atlasObj['w'] * scale * self.scaleX,
			self.atlasObj['h'] * scale
		);
		var cornerNW = new THREE.Vector2( cornerNE.x * -1.0, cornerNE.y );
		rotateVec2( cornerNE, rotFunc(firstRollAngle) );
		rotateVec2( cornerNW, rotFunc(firstRollAngle) );

		var pos = drawIdx * 4 * 3;
		spritePositions[pos  ] = placement.x + cornerNW.x;
		spritePositions[pos+1] = placement.y + cornerNW.y;
		spritePositions[pos+3] = placement.x + cornerNE.x;
		spritePositions[pos+4] = placement.y + cornerNE.y;
		spritePositions[pos+6] = placement.x - cornerNE.x;
		spritePositions[pos+7] = placement.y - cornerNE.y;
		spritePositions[pos+9] = placement.x - cornerNW.x;
		spritePositions[pos+10] = placement.y - cornerNW.y;

		spritePositions[pos+2] = spritePositions[pos+5] = spritePositions[pos+8] = spritePositions[pos+11] = placement.z;

		// Update UVs
		var uvOffset = drawIdx * 4 * 2;
		//spriteUVs.set( self.atlasObj['uvs'], uvOffset );	// error in Chrome: "Source is too large" ???
		for( var i=0; i<8; i++ ) {
			spriteUVs[uvOffset+i] = self.atlasObj['uvs'][i];
		}
		*/

		/*
		self.plane.position.copy( placement );
		self.plane.rotation.set( 0, 0, rotFunc(firstRollAngle) );
		self.plane.scale.set( scale * self.scaleX, scale, scale );
		*/

		return true;
	}
}
