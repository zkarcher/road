const IDEAL_BG_SPRITE_Y = 150;

function BackgroundSprite( scene, texture, w, h, params ){
	var self = this;

	if( !params ) params = {};

	self.loc = new THREE.Vector3( 0, IDEAL_BG_SPRITE_Y, 999 );

	var geom = new THREE.PlaneGeometry( w, h, 1, 1 );

	var mat;
	if( params['shaderMaterial'] ) {
		mat = params['shaderMaterial'];

	} else {
		// Three.js doesn't support transparency without also forcing depthTest and depthWrite :P
		var mat = new THREE.MeshBasicMaterial( {color: 0xffffff, side: THREE.DoubleSide, map: texture, transparent:true, depthTest:true, depthWrite:false} );
	}

	self.plane = new THREE.Mesh( geom, mat );
	self.plane.renderOrder = 0;

	scene.add( self.plane );

	self.update = function( panX, panY, dz, roadPositionAr, firstRollAngle, timeMult ) {
		self.loc.x = lerp( 0, self.loc.x + panX*0.07, 1.0 - (0.004*timeMult) );
		self.loc.y = IDEAL_BG_SPRITE_Y + panY*(512*0.5);

		/*
		self.loc.z += dz;

		// After passing the camera, reset Z somewhere in the distance
		if( self.loc.z > 0 ) {
			self.loc.setZ( randRange(-100,-80) );
		}
		*/

		/*
		// Find the roadPosition that matches our Z. We'll plant the Sprite there.
		var placement = new THREE.Vector3();

		var prevPos = roadPositionAr[0];
		for( var i=0, len=roadPositionAr.length; i<len; i++ ) {
			var nextPos = roadPositionAr[i];
			if( nextPos.z < self.loc.z ) {

				// Interpolate between lastPos and pos, get nice position
				function getScreenPosFromRoadPos( pos ) {
					var screenRatio = ((self.loc.x - pos.centerX) / (-self.loc.z)) * pos.totalRollCos + 0.5;	// 0..1, 0.5==center
					return new THREE.Vector3(
						1000 * (screenRatio-0.5),
						lerp( pos.drawY_L, pos.drawY_R, screenRatio ) + (h*0.4)*scale,
						self.loc.z
					);
				}

				var lerpRatio = (self.loc.z - nextPos.z) / (prevPos.z - nextPos.z);
				placement.lerpVectors( getScreenPosFromRoadPos(nextPos), getScreenPosFromRoadPos(prevPos), lerpRatio );

				//console.log( pos.drawY_L, pos.drawY_R, screenRatio, lerp( pos.drawY_L, pos.drawY_R, screenRatio ) );
				break;
			}

			prevPos = nextPos;
		}

		self.plane.position.copy( placement );
		*/

		self.plane.position.set(
			self.loc.x * Math.cos( firstRollAngle ) - self.loc.y * Math.sin( firstRollAngle ),
			self.loc.y * Math.cos( firstRollAngle ) + self.loc.x * Math.sin( firstRollAngle ),
			self.loc.z
		);
		self.plane.rotation.set( 0, 0, firstRollAngle );
		self.plane.scale.set( 1.0, 1.0, 1.0 );

	}
}
