const atlasScales = {
	'grave_doomstone': 0.8,
	'grave_cross': 0.8,
	'grave_angel_weeping': 0.7,
	//'grave_crypt': 0.8,
	'grave_tombstone': 0.8,
	'tree': 1.2,
};
const atlasKeys = Object.keys( atlasScales );

var spriteAtlas = {};	// TX atlas lookups. key: sprite name. val: {x:_, y:_, w:_, h:_}

function Sprite( inEffect, inScale, inLoc, atlasObj, owner ){
	var self = this;
	self.effect = inEffect;	// 'normal', 'balefire', etc
	self.scale = inScale;
	self.loc = inLoc;
	self.atlasObj = atlasObj;
	self.hasOwner = !!owner;

	// Optimization: Used by RoadView.perFrame()
	self.isInView = false;	// is currently visible?

	var w = atlasObj['w'];
	var h = atlasObj['h'];

	// Randomly flip horizontal
	self.scaleX = maybe() ? 1 : -1;

	self.setZ = function( newZ ) {
		self.loc.setZ( newZ );
	}

	self.addZ = function( dz ) {
		self.loc.z += dz;
	}

	self.updateDZ = function( dz ) {
		self.loc.z += dz;

		// After passing the camera, reset Z somewhere in the distance
		// If this sprite is owned, the owner will handle it
		/*
		if( !self.hasOwner && self.loc.z > 0 ) {
			self.setZ( randRange(-100,-80) );
		}
		*/
	}

	self.update = function( drawIdx, spritePositions, spriteUVs, cameraFar, roadPositionAr, firstRollAngle, precision ) {
		if( self.loc.z < cameraFar ) return false;

		var scale = self.scale / (-self.loc.z);

		if( precision===PRECISION_1980S ) {
			scale = Math.max( 0.01, Math.floor( scale*50 ) * 0.02 );
			scale = Math.min( 0.33, scale );
		} else if( precision===PRECISION_1990S ) {
			scale = Math.max( 0.01, Math.floor( scale*200 ) * 0.005 );
		}

		var locFunc = function(n){ return n; }
		if( precision===PRECISION_1980S ) {
			locFunc = function(n){ return Math.round(n*0.25) * 4.0 };
		} else if( precision===PRECISION_1990S ) {
			locFunc = function(n){ return Math.round(n) };
		}

		// Find the roadPosition that matches our Z. We'll plant the Sprite there.
		var placement = new THREE.Vector3();

		var prevPos = roadPositionAr[0];
		for( var i=0, len=roadPositionAr.length; i<len; i++ ) {
			var nextPos = roadPositionAr[i];
			if( nextPos.z < self.loc.z ) {

				// Interpolate between lastPos and pos, get nice position
				function getScreenPosFromRoadPos( pos ) {
					var screenRatio = ((self.loc.x - pos.centerX) / (-self.loc.z)) * pos.totalRollCos + 0.5;	// 0..1, 0.5==center
					return new THREE.Vector3(
						locFunc( 1000 * (screenRatio-0.5) ),
						locFunc( lerp( pos.drawY_L, pos.drawY_R, screenRatio ) + (h*0.4)*scale + self.loc.y/(-self.loc.z) ),
						self.loc.z
					);
				}

				var lerpRatio = (self.loc.z - nextPos.z) / (prevPos.z - nextPos.z);
				placement.lerpVectors( getScreenPosFromRoadPos(nextPos), getScreenPosFromRoadPos(prevPos), lerpRatio );

				//console.log( pos.drawY_L, pos.drawY_R, screenRatio, lerp( pos.drawY_L, pos.drawY_R, screenRatio ) );
				break;
			}

			prevPos = nextPos;
		}

		var rotFunc = function(n){ return n; }
		if( precision===PRECISION_1980S ) {
			rotFunc = function(n){ return Math.round(n*4.0) / 4.0 };
		} else if( precision===PRECISION_1990S ) {
			rotFunc = function(n){ return Math.round(n*16.0) / 16.0 };
		}

		// Update positions
		scale *= 0.5;
		var cornerNE = new THREE.Vector2(
			self.atlasObj['w'] * scale * self.scaleX,
			self.atlasObj['h'] * scale
		);
		var cornerNW = new THREE.Vector2( cornerNE.x * -1.0, cornerNE.y );
		rotateVec2( cornerNE, rotFunc(firstRollAngle) );
		rotateVec2( cornerNW, rotFunc(firstRollAngle) );

		var pos = drawIdx * 4 * 3;
		spritePositions[pos  ] = placement.x + cornerNW.x;
		spritePositions[pos+1] = placement.y + cornerNW.y;
		spritePositions[pos+3] = placement.x + cornerNE.x;
		spritePositions[pos+4] = placement.y + cornerNE.y;
		spritePositions[pos+6] = placement.x - cornerNE.x;
		spritePositions[pos+7] = placement.y - cornerNE.y;
		spritePositions[pos+9] = placement.x - cornerNW.x;
		spritePositions[pos+10] = placement.y - cornerNW.y;

		spritePositions[pos+2] = spritePositions[pos+5] = spritePositions[pos+8] = spritePositions[pos+11] = placement.z;

		// Update UVs
		var uvOffset = drawIdx * 4 * 2;
		//spriteUVs.set( self.atlasObj['uvs'], uvOffset );	// error in Chrome: "Source is too large" ???
		for( var i=0; i<8; i++ ) {
			spriteUVs[uvOffset+i] = self.atlasObj['uvs'][i];
		}

		/*
		self.plane.position.copy( placement );
		self.plane.rotation.set( 0, 0, rotFunc(firstRollAngle) );
		self.plane.scale.set( scale * self.scaleX, scale, scale );
		*/

		return true;
	}

	self.destroy = function() {
		
	}
}
